<?php
namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use ShortLinkApiBundle\Projection\ShortLink\ShortLinkProjector;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170519175727 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        ShortLinkProjector::createTable($schema);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        ShortLinkProjector::dropTable($schema);
    }
}
