<?php
namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use ShortLinkApiBundle\Service\NextInteger\DatabaseNextInteger;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170510204627 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        DatabaseNextInteger::createTable($schema);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        DatabaseNextInteger::dropTable($schema);
    }
}
