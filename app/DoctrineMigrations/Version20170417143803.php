<?php
namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use ShortLinkApiBundle\Projection\Site\SiteProjector;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170417143803 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        SiteProjector::createTable($schema);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        SiteProjector::dropTable($schema);
    }
}
