shlk-in API
===========

[![build status](https://gitlab.com/guyradford/shlk-in/badges/master/build.svg)](https://gitlab.com/guyradford/shlk-in/commits/master) [![coverage report](https://gitlab.com/guyradford/shlk-in/badges/master/coverage.svg)](https://gitlab.com/guyradford/shlk-in/commits/master)


Initialising the site
---------------------

1. Create database: ```bin/console doctrine:database:create```
2. Setup database: ```bin/console doctrine:migrations:migrate```
3. Setup Test Api User: ```bin/console auth:create-user testUser```
4. Setup Api User: ```bin/console auth:create-user shortLinkApi```




## Testing

Start and log into the php container.

```bash
docker-compose up --build -d
docker-compose exec php bash
```

Run tests and linting

```bash
vendor/bin/phpunit
vendor/bin/php-cs-fixer -v fix
vendor/bin/phpunit --coverage-html ./coverage
```


Start Docker 

```bash
docker-compose up --build -d
```


SSH into the php docker container

```bash
docker-compose up --build -d
docker-compose exec php bash
docker-compose exec php composer update
docker-compose exec php composer install
docker-compose exec php composer require
docker-compose exec php php vendor/bin/php-cs-fixer -v fix
docker-compose exec php php vendor/bin/phpunit
docker-compose exec php php vendor/bin/phpunit --coverage-html ./coverage


```


SSH into the mysql docker container

```bash
docker-compose exec db bash
docker-compose exec db mysql -uroot -p"root"
\u shlkin_db
show tables;
select * from site_list;
```
