<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 17/04/2017
 * Time: 15:25
 */
namespace ShortLinkApiBundle\Projection\Site;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use ShortLinkApiBundle\Model\Site\Event\SiteWasCreated;

final class SiteProjector
{
    const TABLE = 'site_list';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param SiteWasCreated $event
     *
     * @return void
     */
    public function onSiteWasCreated(SiteWasCreated $event)
    {

        // Remove other reminder for site first
        $this->connection->delete(
            self::TABLE,
            [
                'site_id' => $event->siteId()->toNative(),
            ]
        );

        if ($event->active()) {
            $this->connection->insert(
                self::TABLE,
                [
                    'site_id' => $event->siteId()->toNative(),
                    'hostname' => $event->hostname()->toNative(),
                    'active' => $event->active()
                ]
            );
        }
    }

    /**
     * Create tables required for projection
     *
     * @param Schema $schema
     */
    public static function createTable(Schema $schema)
    {
        $siteProjection = $schema->createTable(self::TABLE);

        $siteProjection->addColumn('site_id', 'string', ['fixed' => true, 'length' => 36]);
        $siteProjection->addColumn('hostname', 'string', ['length' => 255]);
        $siteProjection->addColumn('active', 'boolean', ['default' => false]);

        $siteProjection->setPrimaryKey(['site_id']);
        $siteProjection->addUniqueIndex(['hostname'], self::TABLE . '_hostname');
    }

    /**
     * Drop projection table
     *
     * @param Schema $schema
     */
    public static function dropTable(Schema $schema)
    {
        $schema->dropTable(self::TABLE);
    }
}
