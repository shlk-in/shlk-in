<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 17/04/2017
 * Time: 15:25
 */
namespace ShortLinkApiBundle\Projection\Site;

use Doctrine\DBAL\Connection;
use GuyRadford\ValueObject\Hostname;

final class SiteFinder
{
    /**
     * @var Connection
     */
    private $connection;
    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
        $this->connection->setFetchMode(\PDO::FETCH_OBJ);
    }

    /**
     * @return \stdClass[] of siteData
     */
    public function findAll()
    {
        return $this->connection->fetchAll(sprintf('SELECT * FROM %s', SiteProjector::TABLE));
    }

    /**
     * Return siteDate searching on the hostname.
     *
     * @param Hostname $hostname
     *
     * @return \stdClass[] of siteData
     */
    public function findActiveSiteByHostname(Hostname $hostname)
    {
        return $this
            ->connection
            ->fetchAll(sprintf("SELECT * FROM %s WHERE active=true AND hostname LIKE '%s'", SiteProjector::TABLE, $hostname->toNative()));
    }

//    /**
//     * @param string $assigneeId
//     * @return \stdClass[] of todoData
//     */
//    public function findByAssigneeId($assigneeId)
//    {
//        return $this->connection->fetchAll(
//            sprintf('SELECT * FROM %s WHERE assignee_id = :assignee_id', Table::TODO),
//            ['assignee_id' => $assigneeId]
//        );
//    }
//
//    /**
//     * @param string $todoId
//     * @return \stdClass of todoData
//     */
//    public function findById($todoId)
//    {
//        $stmt = $this->connection->prepare(sprintf('SELECT * FROM %s where id = :todo_id', Table::TODO));
//        $stmt->bindValue('todo_id', $todoId);
//        $stmt->execute();
//        return $stmt->fetch();
//    }
//
//    /**
//     * @return \stdClass[] of todoData
//     */
//    public function findByOpenReminders()
//    {
//        $stmt = $this
//            ->connection
//            ->prepare(sprintf('SELECT * FROM %s where reminder < NOW() AND reminded = 0', Table::TODO));
//        $stmt->execute();
//
//        return $stmt->fetchAll();
//    }
//
//    /**
//     * @return \stdClass[] of todoData
//     */
//    public function findOpenWithPastTheirDeadline()
//    {
//        return $this->connection->fetchAll(
//            sprintf(
//                "SELECT * FROM %s WHERE status = :status AND deadline < CONVERT_TZ(NOW(), @@session.time_zone, '+00:00')",
//                Table::TODO
//            ),
//            ['status' => TodoStatus::OPEN]
//        );
//    }
}
