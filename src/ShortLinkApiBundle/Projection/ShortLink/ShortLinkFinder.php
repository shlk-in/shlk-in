<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 17/04/2017
 * Time: 15:25
 */
namespace ShortLinkApiBundle\Projection\ShortLink;

use Doctrine\DBAL\Connection;
use ShortLinkApiBundle\Model\ShortLink\Exception\ShortLinkNotFound;
use ShortLinkApiBundle\ValueObject\ShortLinkCode;
use ShortLinkApiBundle\ValueObject\SiteId;

final class ShortLinkFinder
{
    /**
     * @var Connection
     */
    private $connection;
    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
        $this->connection->setFetchMode(\PDO::FETCH_OBJ);
    }

    /**
     * @return \stdClass[] of siteData
     */
    public function findAll()
    {
        return $this->connection->fetchAll(sprintf('SELECT * FROM %s', ShortLinkProjector::TABLE));
    }

    /**
     * Return siteDate searching on the hostname.
     *
     * @param SiteId $siteId
     * @param ShortLinkCode $shortLinkCode
     *
     * @return \stdClass[] of siteData
     *
     */
    public function findShortLinkBySiteIdAndShortLinkCode(SiteId $siteId, ShortLinkCode $shortLinkCode)
    {
        $shortLinkLookup = $this
            ->connection
            ->fetchAll(sprintf("SELECT * FROM %s WHERE site_id = '%s' AND short_link_code LIKE '%s'", ShortLinkProjector::TABLE, $siteId->toNative(), $shortLinkCode->toNative()));

        if (empty($shortLinkLookup)) {
            throw ShortLinkNotFound::withShortLinkCode($shortLinkCode);
        }

        return $shortLinkLookup;
    }
}
