<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 17/04/2017
 * Time: 15:25
 */
namespace ShortLinkApiBundle\Projection\ShortLink;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use ShortLinkApiBundle\Model\ShortLink\Event\ShortLinkWasCreated;

final class ShortLinkProjector
{
    const TABLE = 'short_link_lookup';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param ShortLinkWasCreated $event
     *
     * @return void
     */
    public function onShortLinkWasCreated(ShortLinkWasCreated $event)
    {

        // Remove other reminder for site first
        $this->connection->delete(
            self::TABLE,
            [
                'short_link_id' => $event->shortLinkId()->toNative(),
            ]
        );

        $this->connection->insert(
            self::TABLE,
            [
                'short_link_id' => $event->shortLinkId()->toNative(),
                'site_id'       => $event->siteId()->toNative(),
                'short_link_code' => $event->shortLinkCode()->toNative(),
            ]
        );
    }

    /**
     * Create tables required for projection
     *
     * @param Schema $schema
     */
    public static function createTable(Schema $schema)
    {
        $siteProjection = $schema->createTable(self::TABLE);

        $siteProjection->addColumn('short_link_id', 'guid');
        $siteProjection->addColumn('site_id', 'guid');
        $siteProjection->addColumn('short_link_code', 'string', ['length' => 20]);

        $siteProjection->setPrimaryKey(['short_link_id']);
        $siteProjection->addUniqueIndex(['site_id', 'short_link_code'], self::TABLE . '_site_id_short_link_code');
    }

    /**
     * Drop projection table
     *
     * @param Schema $schema
     */
    public static function dropTable(Schema $schema)
    {
        $schema->dropTable(self::TABLE);
    }
}
