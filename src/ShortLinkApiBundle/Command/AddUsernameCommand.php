<?php
/**overwritten*/
namespace ShortLinkApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddUsernameCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('auth:create-user')
            ->addArgument('username', InputArgument::REQUIRED, 'The username of the api user.')

            // the short description shown while running "php bin/console list"
            ->setDescription('Creates a new. API User');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'User Creator',
            '============',
            '',
        ]);

        $user = $this->getContainer()->get('doctrine')->getRepository('ShortLinkApiBundle:ApiUser')
            ->findOneBy(['username' => $input->getArgument('username')]);

        if ($user) {
            $output->writeln('<error>User \'' . $input->getArgument('username') . '\' already exists!</error>');

            return;
        }

        $output->writeln('Creating Api User : ' . $input->getArgument('username'));

        $connection = $this->getContainer()->get('doctrine.orm.default_entity_manager');

        $queryBuilder = $connection->getConnection()->createQueryBuilder();

        $queryBuilder
            ->insert('api_user')
            ->values(
                [
                    'username' => '?'
                ]
            )
            ->setParameter(0, $input->getArgument('username'));
        $queryBuilder->execute();

        $output->writeln('Finished.');
    }
}
