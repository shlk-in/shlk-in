<?php
/**overwritten*/
namespace ShortLinkApiBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Process\Process;

class InitJwtCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('init:jwt')
            // the short description shown while running "php bin/console list"
            ->setDescription('Creates public/private keys for JWT.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(
            [
                'Create new Public and Private SSH Keys for JWT',
                '==============================================',
                '',
            ]
        );

        $fs = $this->getContainer()->get('oneup_flysystem.var_filesystem');

        //check if keys already exist?
        if ($this->doSshKeysAlreadyExists()) {
            $output->writeln('<info>You already have JWT SSH Keys.</info>');

            if ($input->isInteractive() && !$this->confirm($input, $output, 'Confirm overwrite of current SSH Keys? (y/N) ')) {
                $output->writeln('<info>SSH Keys NOT overwritten!</info>');

                return;
            }

            // delete old keys
            $output->writeln('Deleting old SSH keys...');
            $fs->delete($this->getContainer()->getParameter('jwt_private_key_path'));
            $fs->delete($this->getContainer()->getParameter('jwt_public_key_path'));
        }

        $folder = dirname($this->getContainer()->getParameter('jwt_private_key_path'));

        if (!$fs->has($folder)) {
            $output->writeln('Creating folder...');
            $fs->createDir($folder);
        }

        $output->writeln('Create new private key...');
//        openssl genrsa -out app/var/jwt/private.pem -aes256 4096
        $command = sprintf(
            'openssl genrsa -out %s -aes256 -passout pass:%s 4096',
            $this->getContainer()->getParameter('jwt_private_key_path'),
            $this->getContainer()->getParameter('jwt_key_pass_phrase')
        );

        $this->runCommand($output, $command);

        $output->writeln('Create new public key...');
//      openssl rsa -pubout -in app/var/jwt/private.pem -out app/var/jwt/public.pem
        $command = sprintf(
            'openssl rsa -pubout -in  %s -out %s -passin pass:%s',
            $this->getContainer()->getParameter('jwt_private_key_path'),
            $this->getContainer()->getParameter('jwt_public_key_path'),
            $this->getContainer()->getParameter('jwt_key_pass_phrase')
        );
        $this->runCommand($output, $command);

        $output->writeln('Finished.');
    }

    protected function doSshKeysAlreadyExists()
    {
        $fs = $this->getContainer()->get('oneup_flysystem.var_filesystem');

        if (
            $fs->has($this->getContainer()->getParameter('jwt_private_key_path'))
            &&
            $fs->has($this->getContainer()->getParameter('jwt_public_key_path'))
        ) {
            return true;
        }

        return false;
    }

    protected function confirm(InputInterface $input, OutputInterface $output, $message)
    {
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion($message, false);

        return $helper->ask($input, $output, $question);
    }

    /**
     * @param OutputInterface $output
     * @param string $command
     */
    protected function runCommand(OutputInterface $output, $command)
    {
        $process = new Process($command);
        $process->run();
    }
}
