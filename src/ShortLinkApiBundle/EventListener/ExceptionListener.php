<?php
namespace ShortLinkApiBundle\EventListener;

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 12/06/2017
 * Time: 21:31
 */
use Prooph\ServiceBus\Exception\MessageDispatchException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {

        // You get the exception object from the received event
        $exception = $event->getException();

        if ($exception instanceof MessageDispatchException) {
            $exception = $exception->getPrevious();
        }

//        $message = sprintf(
//            'My Error says: %s with code: %s',
//            $exception->getMessage(),
//            $exception->getCode()
//        );
//
//        // Customize your response object to display the exception details
//        $response = new Response();
//        $response->setContent($message);

        $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        $headers = [];

        if ($exception instanceof \GuyRadford\ValueObject\Exception\InvalidArgumentException) {
            $statusCode =  400;
        }

        if ($exception instanceof HttpExceptionInterface) {
            $statusCode =  $exception->getStatusCode();
            $headers = $exception->getHeaders();
        }
        $response = new JsonResponse([
            'success' => false,
            'error' => [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
            ]
        ], $statusCode, $headers);

        // HttpExceptionInterface is a special type of exception that
        // holds status code and header details
//        if ($exception instanceof HttpExceptionInterface) {
//            $response->setStatusCode($exception->getStatusCode());
//            $response->headers->replace($exception->getHeaders());
//        } else {
//            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
//        }

        // Send the modified response object to the event
        $event->setResponse($response);
    }
}
