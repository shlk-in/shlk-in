<?php
namespace ShortLinkApiBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 23/06/2017
 * Time: 20:13
 */
class AuthenticationFailed extends HttpException
{
    /**
     * @param string $username
     *
     * @return static
     *
     */
    public static function withSuppliedUsername($username)
    {
        return new self(401, sprintf('Can not authenticate with username \'%s\'.', $username));
    }

    /**
     * @param string $message
     *
     * @return static
     */
    public static function withMessage($message)
    {
        return new self(401, $message);
    }
}
