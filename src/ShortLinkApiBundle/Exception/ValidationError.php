<?php
namespace ShortLinkApiBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 23/06/2017
 * Time: 20:13
 */
class ValidationError extends HttpException
{
}
