<?php
namespace ShortLinkApiBundle\Exception;

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 23/06/2017
 * Time: 20:13
 */
class MissingHeader extends ValidationError
{
    /**
     * @return static
     */
    public static function missingAuthorizationHeader()
    {
        return new self(401, sprintf('Authorization header is required.'));
    }
}
