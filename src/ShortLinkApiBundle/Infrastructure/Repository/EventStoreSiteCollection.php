<?php
namespace ShortLinkApiBundle\Infrastructure\Repository;

use Prooph\EventStore\Aggregate\AggregateRepository;
use ShortLinkApiBundle\Model\Site\Exception\SiteNotFound;
use ShortLinkApiBundle\Model\Site\Site;
use ShortLinkApiBundle\Model\Site\SiteCollection;
use ShortLinkApiBundle\ValueObject\SiteId;

/**
 * Class EventStoreSendCollection
 *
 * @package Application\Infrastructure\Repository
 *
 * @author Alexander Miertsch <kontakt@codeliner.ws>
 */
final class EventStoreSiteCollection extends AggregateRepository implements SiteCollection
{
    /**
     * @param Site $site
     *
     * @return void
     */
    public function add(Site $site)
    {
        $this->addAggregateRoot($site);
    }

    /**
     * @param SiteId $sendId
     *
     * @return Site
     */
    public function get(SiteId $sendId)
    {
        $site = $this->getAggregateRoot($sendId->toNative());

        if (empty($site)) {
            throw SiteNotFound::withSiteId($sendId);
        }

        return $site;
    }
}
