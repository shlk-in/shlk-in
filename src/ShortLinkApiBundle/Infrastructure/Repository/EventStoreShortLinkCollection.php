<?php
namespace ShortLinkApiBundle\Infrastructure\Repository;

use Prooph\EventStore\Aggregate\AggregateRepository;
use ShortLinkApiBundle\Model\ShortLink\Exception\ShortLinkNotFound;
use ShortLinkApiBundle\Model\ShortLink\ShortLink;
use ShortLinkApiBundle\Model\ShortLink\ShortLinkCollection;
use ShortLinkApiBundle\Model\Site\Site;
use ShortLinkApiBundle\ValueObject\ShortLinkId;

/**
 * Class EventStoreSendCollection
 *
 * @package Application\Infrastructure\Repository
 *
 * @author Alexander Miertsch <kontakt@codeliner.ws>
 */
final class EventStoreShortLinkCollection extends AggregateRepository implements ShortLinkCollection
{
    /**
     * @param ShortLink $shortLink
     *
     */
    public function add(ShortLink $shortLink)
    {
        $this->addAggregateRoot($shortLink);
    }

    /**
     * @param ShortLinkId $shortLinkId
     *
     * @return Site
     */
    public function get(ShortLinkId $shortLinkId)
    {
        $shortLink = $this->getAggregateRoot($shortLinkId->toNative());

        if (empty($shortLink)) {
            throw ShortLinkNotFound::withShortLinkId($shortLinkId);
        }

        return $shortLink;
    }
}
