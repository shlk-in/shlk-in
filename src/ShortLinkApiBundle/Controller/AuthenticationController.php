<?php
namespace ShortLinkApiBundle\Controller;

use ShortLinkApiBundle\Exception\AuthenticationFailed;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AuthenticationController extends Controller
{
    /**
     * @Route(path="token-authentication", name="token_authentication")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function tokenAuthentication(Request $request)
    {
        $username = $request->request->get('username');
//        $password = $request->request->get('password');

        $user = $this->getDoctrine()->getRepository('ShortLinkApiBundle:ApiUser')
            ->findOneBy(['username' => $username]);

        if (!$user) {
            throw AuthenticationFailed::withSuppliedUsername($username);
        }
//
//        // password check
//        if(!$this->get('security.password_encoder')->isPasswordValid($user, $password)) {
//            throw $this->createAccessDeniedException();
//        }

        // Use LexikJWTAuthenticationBundle to create JWT token that hold only information about user name
        $token = $this->get('lexik_jwt_authentication.encoder')
//            ->encode(['ip_address' => $ipAddress]);
            ->encode(['username' => $user->getUsername()]);

        // Return genereted tocken
        return new JsonResponse(['token' => $token]);
    }
}
