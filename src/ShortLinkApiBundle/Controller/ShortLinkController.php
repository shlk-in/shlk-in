<?php
namespace ShortLinkApiBundle\Controller;

use ShortLinkApiBundle\Model\ShortLink\Command\CreateShortLink;
use ShortLinkApiBundle\Model\ShortLink\Command\ShortLinkVisited;
use ShortLinkApiBundle\ValueObject\LongUrl;
use ShortLinkApiBundle\ValueObject\ShortLinkCode;
use ShortLinkApiBundle\ValueObject\ShortLinkId;
use ShortLinkApiBundle\ValueObject\SiteId;
use ShortLinkApiBundle\ValueObject\VisitDateTime;
use ShortLinkApiBundle\ValueObject\VisitorsIPAddress;
use ShortLinkApiBundle\ValueObject\VisitorsUserAgent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ShortLinkController extends Controller
{
    /**
     * Add new site
     *
     * @Method("POST")
     * @Route("shortLink/")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addAction(Request $request)
    {
        $shortLinkId = ShortLinkId::generate();
        $this->get('prooph_service_bus.shlkin_command_bus')
            ->dispatch(
                CreateShortLink::create(
                    $shortLinkId,
                    SiteId::fromNative($request->request->get('siteId')),
                    LongUrl::fromNative($request->request->get('longUrl'))
                )
            );

        return new JsonResponse(
            [
                'success'     => true,
                'shortLinkId' => $shortLinkId->toNative(),
            ]
        );
    }

    /**
     * Get single Short Link
     *
     * @Route("shortLink/{shortLinkId}")
     *
     * @param $shortLinkId
     *
     * @return JsonResponse
     *
     */
    public function getShortLinkAction($shortLinkId)
    {
        $reader = $this->get('short_link_collection');
        $shortLink = $reader->get(ShortLinkId::fromNative($shortLinkId));

        return new JsonResponse(
            [
                'success'   => true,
                'shortLink' => $shortLink->toArray(),
            ]
        );
    }

    /**
     * get a short URL for a long URL
     * Requires:
     *  site_id
     *  short link code
     *
     * @Route("getShortLink/{siteId}/{shortLnkCode}")
     *
     * @param $siteId
     * @param $shortLnkCode
     *
     * @return JsonResponse
     */
    public function getShortLinkByCodeAction($siteId, $shortLnkCode)
    {
        $shortLinkFinder = $this->container->get('shlkin.short_link_projection.short_link_finder');

        $shortLinks = $shortLinkFinder->findShortLinkBySiteIdAndShortLinkCode(
            SiteId::fromNative($siteId),
            ShortLinkCode::fromNative($shortLnkCode)
        );

        $shortLink = $this->get('short_link_collection')->get(ShortLinkId::fromNative($shortLinks[0]->short_link_id));

        return new JsonResponse(
            [
                'success'   => true,
                'shortLink' => $shortLink->toArray(),
            ]
        );
    }

    /**
     * Records when a short link is entered and redirects to the long link.
     *
     * Requires:
     *  $shortLinkId
     *
     * @Method("POST")
     * @Route("shortLinkVisited/{shortLinkId}")
     *
     * @param Request $request
     * @param $shortLinkId
     *
     * @return JsonResponse
     */
    public function visitedAction(Request $request, $shortLinkId)
    {
        $this->get('prooph_service_bus.shlkin_command_bus')
            ->dispatch(
                ShortLinkVisited::create(
                    ShortLinkId::fromNative($shortLinkId),
                    VisitDateTime::now(),
                    VisitorsIPAddress::fromNative($request->request->get('visitorsIPAddress')),
                    VisitorsUserAgent::fromNative($request->request->get('visitorsUserAgent'))
                )
            );

        return new JsonResponse(
            [
                'success' => true,
            ]
        );
    }
}
