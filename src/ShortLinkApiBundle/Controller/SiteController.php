<?php
namespace ShortLinkApiBundle\Controller;

use GuyRadford\ValueObject\Hostname;
use ShortLinkApiBundle\Model\Site\Command\CreateNewSite;
use ShortLinkApiBundle\Model\Site\Exception\SiteNotFound;
use ShortLinkApiBundle\ValueObject\Active;
use ShortLinkApiBundle\ValueObject\ShortCodeAllowedCharacters;
use ShortLinkApiBundle\ValueObject\ShortCodeMinimumLength;
use ShortLinkApiBundle\ValueObject\ShortCodePassKey;
use ShortLinkApiBundle\ValueObject\SiteId;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class SiteController extends Controller
{
    /**
     * List all sites
     *
     * @Method("GET")
     * @Route("site/")
     *
     * @return JsonResponse
     */
    public function listAction()
    {
        $todoFinder = $this->container->get('shlkin.site_projection.site_finder');

        $sites = $todoFinder->findAll();

        $sitesList = [];
        foreach ($sites as $site) {
            $sitesList[] = $this->get('site_collection')->get(SiteId::fromNative($site->site_id))->toArray();
        }

        return new JsonResponse(
            [
                'success' => true,
                'sites'   => $sitesList,
            ]
        );
    }

    /**
     * Add new site
     *
     * @Method("POST")
     * @Route("site/")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addAction(Request $request)
    {
        $siteId = SiteId::generate();
        $this->get('prooph_service_bus.shlkin_command_bus')
            ->dispatch(
                CreateNewSite::create(
                    $siteId,
                    Hostname::fromNative($request->request->get('hostname')),
                    Active::fromNative($request->request->get('active')),
                    ShortCodeMinimumLength::fromString($request->request->get('shortCodeMinimumLength')),
                    ShortCodePassKey::fromNative($request->request->get('shortCodePassKey')),
                    ShortCodeAllowedCharacters::fromNative($request->request->get('shortCodeAllowedCharacters'))
                )
            );

        return new JsonResponse(
            [
                'success' => true,
                'siteId'  => $siteId->toNative(),
            ]
        );
    }

    /**
     * Get single site
     *
     * @Route("site/{siteId}")
     *
     * @param $siteId
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getSiteAction($siteId)
    {
        $siteId = SiteId::fromNative($siteId);

        $reader = $this->get('site_collection');
        $site = $reader->get($siteId);

        return new JsonResponse(
            [
                'success' => true,
                'site'    => $site->toArray(),
            ]
        );
    }

    /**
     * Find single site
     *
     * @Route("siteFind/")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function findSiteAction(Request $request)
    {
        $hostname = Hostname::fromNative($request->query->get('hostname'));

        $siteFinder = $this->container->get('shlkin.site_projection.site_finder');
        $sites = $siteFinder->findActiveSiteByHostname($hostname);

        if (empty($sites)) {
            throw SiteNotFound::forHostname($hostname);
        }

        $site = $this->get('site_collection')->get(SiteId::fromNative($sites[0]->site_id));

        return new JsonResponse(
            [
                'success' => true,
                'site'    => $site->toArray(),
            ]
        );
    }
}
