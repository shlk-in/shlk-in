<?php
/*
 * This file is part of prooph/proophessor.
 * (c) 2014-2015 prooph software GmbH <contact@prooph.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Date: 5/2/15 - 5:50 PM
 */
namespace ShortLinkApiBundle\Model\ShortLink\Command;

use GuyRadford\ValueObject\Hostname;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use ShortLinkApiBundle\ValueObject\LongUrl;
use ShortLinkApiBundle\ValueObject\ShortLinkId;
use ShortLinkApiBundle\ValueObject\SiteId;

/**
 * Class CreateNewShortLink
 *
 * @package ShortLinkApiBundle\Model\ShortLink
 */
final class CreateShortLink extends Command implements PayloadConstructable
{
    use PayloadTrait;

    /**
     * @param ShortLinkId $shortLinkId
     * @param SiteId $siteId
     * @param LongUrl $longUrl
     *
     * @return CreateShortLink
     */
    public static function create(ShortLinkId $shortLinkId, SiteId $siteId, LongUrl $longUrl)
    {
        return new self(
            [
                'shortLinkId' => $shortLinkId->toNative(),
                'siteId' => $siteId->toNative(),
                'longUrl' => $longUrl->toNative()
            ]
        );
    }

    /**
     * @return ShortLinkId
     */
    public function shortLinkId()
    {
        return ShortLinkId::fromNative($this->payload['shortLinkId']);
    }

    /**
     * @return SiteId
     */
    public function siteId()
    {
        return SiteId::fromNative($this->payload['siteId']);
    }

    /**
     * @return Hostname
     */
    public function longUrl()
    {
        return LongUrl::fromNative($this->payload['longUrl']);
    }
}
