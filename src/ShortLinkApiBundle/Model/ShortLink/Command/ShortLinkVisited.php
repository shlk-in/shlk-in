<?php
/*
 * This file is part of prooph/proophessor.
 * (c) 2014-2015 prooph software GmbH <contact@prooph.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Date: 5/2/15 - 5:50 PM
 */
namespace ShortLinkApiBundle\Model\ShortLink\Command;

use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use ShortLinkApiBundle\ValueObject\ShortLinkId;
use ShortLinkApiBundle\ValueObject\SiteId;
use ShortLinkApiBundle\ValueObject\VisitDateTime;
use ShortLinkApiBundle\ValueObject\VisitorsIPAddress;
use ShortLinkApiBundle\ValueObject\VisitorsUserAgent;

/**
 * Class CreateNewShortLink
 *
 * @package ShortLinkApiBundle\Model\ShortLink
 */
final class ShortLinkVisited extends Command implements PayloadConstructable
{
    use PayloadTrait;

    /**
     * @param ShortLinkId $shortLinkId
     * @param VisitDateTime $visitDateTime
     * @param VisitorsIPAddress $visitorsIPAddress
     * @param VisitorsUserAgent $visitorsUserAgent
     *
     * @return CreateShortLink
     */
    public static function create(ShortLinkId $shortLinkId, VisitDateTime $visitDateTime, VisitorsIPAddress $visitorsIPAddress, VisitorsUserAgent $visitorsUserAgent)
    {
        return new self(
            [
                'shortLinkId' => $shortLinkId->toNative(),
                'visitDateTime' => $visitDateTime->toNative(),
                'visitorsIPAddress' => $visitorsIPAddress->toNative(),
                'visitorsUserAgent' => $visitorsUserAgent->toNative()
            ]
        );
    }

    /**
     * @return ShortLinkId
     */
    public function shortLinkId()
    {
        return ShortLinkId::fromNative($this->payload['shortLinkId']);
    }

    /**
     * @return SiteId
     */
    public function visitDateTime()
    {
        return VisitDateTime::fromNative($this->payload['visitDateTime']);
    }

    /**
     * @return VisitorsIPAddress
     */
    public function visitorsIPAddress()
    {
        return VisitorsIPAddress::fromNative($this->payload['visitorsIPAddress']);
    }

    /**
     * @return VisitorsUserAgent
     */
    public function visitorsUserAgent()
    {
        return VisitorsUserAgent::fromNative($this->payload['visitorsUserAgent']);
    }
}
