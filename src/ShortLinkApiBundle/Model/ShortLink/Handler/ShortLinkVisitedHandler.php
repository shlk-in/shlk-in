<?php
namespace ShortLinkApiBundle\Model\ShortLink\Handler;

use ShortLinkApiBundle\Model\ShortLink\Command\ShortLinkVisited;
use ShortLinkApiBundle\Model\ShortLink\ShortLinkCollection;
use ShortLinkApiBundle\Model\Site\SiteCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CreateNewSiteHandler
 *
 * @package ShortLinkApiBundle\Model\Site\Handler
 */
final class ShortLinkVisitedHandler
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var ShortLinkCollection
     */
    private $shortLinkCollection;
    /**
     * @var SiteCollection
     */
    private $siteCollection;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->shortLinkCollection = $this->container->get('short_link_collection');
        $this->siteCollection = $this->container->get('site_collection');
    }

    /**
     * @param ShortLinkVisited $command
     */
    public function __invoke(ShortLinkVisited $command)
    {
        $shortLink = $this->shortLinkCollection->get($command->shortLinkId());
//        if (!$shortLink) {
//            throw ShortLinkNotFound::withShortLinkId($command->shortLinkId());
//        }
        $shortLink->visitRecorded($command->visitDateTime(), $command->visitorsIPAddress(), $command->visitorsUserAgent());
    }
}
