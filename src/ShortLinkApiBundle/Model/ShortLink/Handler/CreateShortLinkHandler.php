<?php
namespace ShortLinkApiBundle\Model\ShortLink\Handler;

use GuyRadford\ValueObject\Iso8601DateTime;
use ShortLinkApiBundle\Model\ShortLink\Command\CreateShortLink;
use ShortLinkApiBundle\Model\ShortLink\ShortLink;
use ShortLinkApiBundle\Model\ShortLink\ShortLinkCollection;
use ShortLinkApiBundle\Model\Site\SiteCollection;
use ShortLinkApiBundle\ValueObject\ShortLinkCode;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CreateNewSiteHandler
 *
 * @package ShortLinkApiBundle\Model\Site\Handler
 */
final class CreateShortLinkHandler
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var ShortLinkCollection
     */
    private $shortLinkCollection;
    /**
     * @var SiteCollection
     */
    private $siteCollection;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->shortLinkCollection = $this->container->get('short_link_collection');
        $this->siteCollection = $this->container->get('site_collection');
    }

    /**
     * @param CreateShortLink $command
     */
    public function __invoke(CreateShortLink $command)
    {

        //load site model
        $site = $this->siteCollection->get($command->siteId());

        //get next link index
        $shortCodeIndex = $this->container->get('next_integer_service')->getNextInteger($command->siteId());

        //turn id into alpha Id
        $shortCodeGenerator = $this->container->get('short_code_generator');
        $shortLinkCode = ShortLinkCode::fromNative(
            $shortCodeGenerator->indexToShortCode(
                $shortCodeIndex,
                $site->ShortCodeMinimumLength()->toNative(),
                $site->ShortCodePassKey()->toNative(),
                $site->ShortCodeAllowedCharacters()->toNative()
            )
        );

        //get the short Url
        $shortCodeUrl = $site->getShortUrl($shortLinkCode);

        $shortLink = ShortLink::createNew(
            $command->shortLinkId(),
            $command->siteId(),
            $command->longUrl(),
            $shortLinkCode,
            $shortCodeUrl,
            Iso8601DateTime::now()
        );

        $this->shortLinkCollection->add($shortLink);
    }
}
