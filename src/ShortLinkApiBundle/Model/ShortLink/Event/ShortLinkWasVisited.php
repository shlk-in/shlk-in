<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 10/06/2017
 * Time: 16:39
 */
namespace ShortLinkApiBundle\Model\ShortLink\Event;

use Prooph\EventSourcing\AggregateChanged;
use ShortLinkApiBundle\ValueObject\ShortLinkId;
use ShortLinkApiBundle\ValueObject\VisitDateTime;
use ShortLinkApiBundle\ValueObject\VisitorsIPAddress;
use ShortLinkApiBundle\ValueObject\VisitorsUserAgent;

class ShortLinkWasVisited extends AggregateChanged
{
    /**
     * @var ShortLinkId
     */
    private $shortLinkId;

    /**
     * @var VisitDateTime
     */
    private $visitDateTime;

    /**
     * @var VisitorsIPAddress
     */
    private $visitorsIPAddress;

    /**
     * @var VisitorsUserAgent
     */
    private $visitorsUserAgent;

    /**
     * @param ShortLinkId $shortLinkId
     * @param VisitDateTime $visitDateTime
     * @param VisitorsIPAddress $visitorsIPAddress
     * @param VisitorsUserAgent $visitorsUserAgent
     *
     * @return ShortLinkWasCreated
     */
    public static function at(ShortLinkId $shortLinkId, VisitDateTime $visitDateTime, VisitorsIPAddress $visitorsIPAddress, VisitorsUserAgent $visitorsUserAgent)
    {
        $event = self::occur(
            $shortLinkId->toNative(),
            [
                'visitDateTime' => $visitDateTime->toNative(),
                'visitorsIPAddress' => $visitorsIPAddress->toNative(),
                'visitorsUserAgent' => $visitorsUserAgent->toNative()
            ]
        );

        $event->shortLinkId = $shortLinkId;
        $event->visitDateTime = $visitDateTime;
        $event->visitorsIPAddress = $visitorsIPAddress;
        $event->visitorsUserAgent = $visitorsUserAgent;

        return $event;
    }

    /**
     * @return ShortLinkId
     */
    public function shortLinkId()
    {
        if ($this->shortLinkId === null) {
            $this->shortLinkId = ShortLinkId::fromNative($this->aggregateId());
        }

        return $this->shortLinkId;
    }

    /**
     * @return VisitDateTime
     */
    public function visitDateTime()
    {
        if ($this->visitDateTime === null) {
            $this->visitDateTime = VisitDateTime::fromNative($this->payload['visitDateTime']);
        }

        return $this->visitDateTime;
    }

    /**
     * @return VisitorsIPAddress
     */
    public function visitorsIPAddress()
    {
        if ($this->visitorsIPAddress === null) {
            $this->visitorsIPAddress = VisitorsIPAddress::fromNative($this->payload['visitorsIPAddress']);
        }

        return $this->visitorsIPAddress;
    }

    /**
     * @return VisitorsUserAgent
     */
    public function visitorsUserAgent()
    {
        if ($this->visitorsUserAgent === null) {
            $this->visitorsUserAgent = VisitorsUserAgent::fromNative($this->payload['visitorsUserAgent']);
        }

        return $this->visitorsUserAgent;
    }
}
