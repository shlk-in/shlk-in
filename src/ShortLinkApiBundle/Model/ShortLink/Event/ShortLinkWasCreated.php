<?php
namespace ShortLinkApiBundle\Model\ShortLink\Event;

use GuyRadford\ValueObject\Iso8601DateTime;
use Prooph\EventSourcing\AggregateChanged;
use ShortLinkApiBundle\Model\Site\Site;
use ShortLinkApiBundle\ValueObject\LongUrl;
use ShortLinkApiBundle\ValueObject\ShortUrl;
use ShortLinkApiBundle\ValueObject\ShortLinkCode;
use ShortLinkApiBundle\ValueObject\ShortLinkId;
use ShortLinkApiBundle\ValueObject\SiteId;

/**
 * Class SiteWasCreated
 *
 * @package ShortLinkApiBundle\Model\Site\Event
 */
final class ShortLinkWasCreated extends AggregateChanged
{
    /**
     * @var ShortLinkId
     */
    private $shortLinkId;

    /**
     * @var Site
     */
    private $siteId;

    /**
     * @var LongUrl
     */
    private $longUrl;

    /**
     * @var ShortLinkCode
     */
    private $shortLinkCode;

    /**
     * @var ShortUrl
     */
    private $shortUrl;

    /**
     * @var Iso8601DateTime
     */
    private $createdAtDateTime;

    /**
     * @param ShortLinkId $shortLinkId
     * @param SiteId $siteId
     * @param LongUrl $longUrl
     * @param ShortLinkCode $shortLinkCode
     * @param ShortUrl $shortUrl
     * @param Iso8601DateTime $createdAtDateTime
     *
     * @return ShortLinkWasCreated
     */
    public static function withData(ShortLinkId $shortLinkId, SiteId $siteId, LongUrl $longUrl, ShortLinkCode $shortLinkCode, ShortUrl $shortUrl, Iso8601DateTime $createdAtDateTime)
    {
        $event = self::occur(
            $shortLinkId->toNative(),
            [
                'siteId' => $siteId->toNative(),
                'longUrl' => $longUrl->toNative(),
                'shortLinkCode' => $shortLinkCode->toNative(),
                'shortUrl' => $shortUrl->toNative(),
                'createdAtDateTime' => $createdAtDateTime->toNative(),
            ]
        );

        $event->shortLinkId = $shortLinkId;
        $event->siteId = $siteId;
        $event->longUrl = $longUrl;
        $event->shortLinkCode = $shortLinkCode;
        $event->shortUrl = $shortUrl;
        $event->createdAtDateTime = $createdAtDateTime;

        return $event;
    }

    /**
     * @return ShortLinkId
     */
    public function shortLinkId()
    {
        if ($this->shortLinkId === null) {
            $this->shortLinkId = ShortLinkId::fromNative($this->aggregateId());
        }

        return $this->shortLinkId;
    }

    /**
     * @return SiteId
     */
    public function siteId()
    {
        if ($this->siteId === null) {
            $this->siteId = SiteId::fromNative($this->payload['siteId']);
        }

        return $this->siteId;
    }

    /**
     * @return LongUrl
     */
    public function longUrl()
    {
        if ($this->longUrl === null) {
            $this->longUrl = LongUrl::fromNative($this->payload['longUrl']);
        }

        return $this->longUrl;
    }

    /**
     * @return ShortLinkCode
     */
    public function shortLinkCode()
    {
        if ($this->shortLinkCode === null) {
            $this->shortLinkCode = ShortLinkCode::fromNative($this->payload['shortLinkCode']);
        }

        return $this->shortLinkCode;
    }

    /**
     * @return ShortUrl
     */
    public function shortUrl()
    {
        if ($this->shortUrl === null) {
            $this->shortUrl = ShortUrl::fromNative($this->payload['shortUrl']);
        }

        return $this->shortUrl;
    }

    /**
     * @return Iso8601DateTime
     */
    public function createdAtDateTime()
    {
        if ($this->createdAtDateTime === null) {
            $this->createdAtDateTime = Iso8601DateTime::fromNative($this->payload['createdAtDateTime']);
        }

        return $this->createdAtDateTime;
    }
}
