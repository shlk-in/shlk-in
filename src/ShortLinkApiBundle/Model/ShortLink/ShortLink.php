<?php
namespace ShortLinkApiBundle\Model\ShortLink;

use GuyRadford\ValueObject\Hostname;
use GuyRadford\ValueObject\IPAddress;
use GuyRadford\ValueObject\Iso8601DateTime;
use GuyRadford\ValueObject\StringLiteral;
use Prooph\EventSourcing\AggregateRoot;
use ShortLinkApiBundle\Model\ShortLink\Event\ShortLinkWasCreated;
use ShortLinkApiBundle\Model\ShortLink\Event\ShortLinkWasVisited;
use ShortLinkApiBundle\ValueObject\LongUrl;
use ShortLinkApiBundle\ValueObject\ShortUrl;
use ShortLinkApiBundle\ValueObject\ShortLinkCode;
use ShortLinkApiBundle\ValueObject\ShortLinkId;
use ShortLinkApiBundle\ValueObject\SiteId;
use ShortLinkApiBundle\ValueObject\VisitDateTime;
use ShortLinkApiBundle\ValueObject\VisitorsIPAddress;
use ShortLinkApiBundle\ValueObject\VisitorsUserAgent;

/**
 * Class Site
 *
 * A user manages Todos on his or her TodoList. Each user is identified by her user id, has a name and an email address.
 *
 * @package ShortLinkApiBundle\Model\Site
 */
final class ShortLink extends AggregateRoot
{
    /**
     * @var ShortLinkId
     */
    private $shortLinkId;

    /**
     * @var SiteId
     */
    private $siteId;

    /**
     * @var LongUrl
     */
    private $longUrl;

    /**
     * @var ShortLinkCode
     */
    private $shortLinkCode;

    /**
     * @var ShortUrl
     */
    private $shortUrl;

    /**
     * @var Iso8601DateTime
     */
    private $createdAtDateTime;

    /**
     * @var int
     */
    private $visitTotal = 0;

    /**
     * @var Iso8601DateTime
     */
    private $dateTimeOfLastVisitor;

    /**
     * @var IPAddress
     */
    private $ipAddressOfLastVisitor;

    /**
     * @var StringLiteral
     */
    private $userAgentOfLastVisitor;

    /**
     * @param ShortLinkId $shortLinkId
     * @param SiteId $siteId
     * @param LongUrl $longUrl
     * @param ShortLinkCode $shortLinkCode
     * @param ShortUrl $shortUrl
     * @param Iso8601DateTime $createdAtDateTime
     *
     * @return ShortLink
     */
    public static function createNew(ShortLinkId $shortLinkId, SiteId $siteId, LongUrl $longUrl, ShortLinkCode $shortLinkCode, ShortUrl $shortUrl, Iso8601DateTime $createdAtDateTime)
    {
        $self = new self();

        $self->recordThat(ShortLinkWasCreated::withData($shortLinkId, $siteId, $longUrl, $shortLinkCode, $shortUrl, $createdAtDateTime));

        return $self;
    }

    /**
     * @param VisitDateTime $visitDateTime
     * @param VisitorsIPAddress $visitorsIPAddress
     * @param VisitorsUserAgent $visitorsUserAgent
     */
    public function visitRecorded(VisitDateTime $visitDateTime, VisitorsIPAddress $visitorsIPAddress, VisitorsUserAgent $visitorsUserAgent)
    {
        $this->recordThat(ShortLinkWasVisited::at($this->shortLinkId, $visitDateTime, $visitorsIPAddress, $visitorsUserAgent));
    }

    /**
     * @return SiteId
     */
    public function SiteId()
    {
        return $this->siteId;
    }

    /**
     * @return Hostname
     */
    public function Hostname()
    {
        return $this->hostname;
    }

//    /**
//     * @param string $text
//     * @param TodoId $todoId
//     * @return Todo
//     */
//    public function postTodo($text, TodoId $todoId)
//    {
//        return Todo::post($text, $this->SiteId(), $todoId);
//    }

    /**
     * @return string representation of the unique identifier of the aggregate root
     */
    protected function aggregateId()
    {
        return $this->shortLinkId->toNative();
    }

    /**
     * @param ShortLinkWasCreated $event
     */
    protected function whenShortLinkWasCreated(ShortLinkWasCreated $event)
    {
        $this->shortLinkId = $event->shortLinkId();
        $this->siteId = $event->siteId();
        $this->longUrl = $event->longUrl();
        $this->shortLinkCode = $event->shortLinkCode();
        $this->shortUrl = $event->shortUrl();
        $this->createdAtDateTime = $event->createdAtDateTime();
    }

    /**
     * @param ShortLinkWasVisited $event
     */
    protected function whenShortLinkWasVisited(ShortLinkWasVisited $event)
    {
        $this->visitTotal ++;
        $this->dateTimeOfLastVisitor = $event->visitDateTime();
        $this->ipAddressOfLastVisitor = $event->visitorsIPAddress();
        $this->userAgentOfLastVisitor = $event->visitorsUserAgent();
    }

    public function toArray()
    {
        return [

            'shortLinkId' => $this->shortLinkId->toNative(),
            'siteId' => $this->siteId->toNative(),
            'longUrl' => $this->longUrl->toNative(),
            'shortUrl' => $this->shortUrl->toNative(),
            'shortLinkCode' => $this->shortLinkCode->toNative(),
            'createdAtDateTime' => $this->createdAtDateTime->toNative(),
            'visitTotal' => $this->visitTotal,
            'dateTimeOfLastVisitor' => is_null($this->dateTimeOfLastVisitor) ? null : $this->dateTimeOfLastVisitor->toNative(),
            'ipAddressOfLastVisitor' => is_null($this->ipAddressOfLastVisitor) ? null : $this->ipAddressOfLastVisitor->toNative(),
            'userAgentOfLastVisitor' => is_null($this->userAgentOfLastVisitor) ? null : $this->userAgentOfLastVisitor->toNative(),
        ];
    }
}
