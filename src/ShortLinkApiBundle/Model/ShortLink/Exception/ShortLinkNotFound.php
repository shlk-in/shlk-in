<?php
namespace ShortLinkApiBundle\Model\ShortLink\Exception;

use ShortLinkApiBundle\Exception\NotFound;
use ShortLinkApiBundle\ValueObject\ShortLinkCode;
use ShortLinkApiBundle\ValueObject\ShortLinkId;

/**
 * Class ShortLinkNotFound
 *
 * @package ShortLinkApiBundle\Model\ShortLink
 */
final class ShortLinkNotFound extends NotFound
{
    /**
     * @param ShortLinkId $shortLinkId
     *
     * @return ShortLinkNotFound
     */
    public static function withShortLinkId(ShortLinkId $shortLinkId)
    {
        return new self(404, sprintf('ShortLink with id %s cannot be found.', $shortLinkId->toNative()));
    }

    /**
     * @param ShortLinkCode $shortLinkCode
     *
     * @return ShortLinkNotFound
     */
    public static function withShortLinkCode(ShortLinkCode $shortLinkCode)
    {
        return new self(404, sprintf('ShortLink with short code %s cannot be found.', $shortLinkCode->toNative()));
    }
}
