<?php
namespace ShortLinkApiBundle\Model\ShortLink;

use ShortLinkApiBundle\ValueObject\ShortLinkId;

/**
 * Interface SiteCollection
 *
 * @package ShortLinkApiBundle\Model\Site
 */
interface ShortLinkCollection
{
    /**
     * @param ShortLink $site
     *
     * @return void
     */
    public function add(ShortLink $site);

    /**
     * @param ShortLinkId $shortLinkId
     *
     * @return ShortLink
     */
    public function get(ShortLinkId $shortLinkId);
}
