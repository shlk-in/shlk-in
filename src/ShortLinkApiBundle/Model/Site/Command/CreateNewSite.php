<?php
/*
 * This file is part of prooph/proophessor.
 * (c) 2014-2015 prooph software GmbH <contact@prooph.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Date: 5/2/15 - 5:50 PM
 */
namespace ShortLinkApiBundle\Model\Site\Command;

use GuyRadford\ValueObject\Hostname;
use GuyRadford\ValueObject\StringLiteral;
use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use ShortLinkApiBundle\ValueObject\Active;
use ShortLinkApiBundle\ValueObject\ShortCodeAllowedCharacters;
use ShortLinkApiBundle\ValueObject\ShortCodeMinimumLength;
use ShortLinkApiBundle\ValueObject\ShortCodePassKey;
use ShortLinkApiBundle\ValueObject\SiteId;

/**
 * Class CreateNewShortLink
 *
 * @package ShortLinkApiBundle\Model\ShortLink
 */
final class CreateNewSite extends Command implements PayloadConstructable
{
    use PayloadTrait;

    /**
     * @param SiteId $siteId
     * @param Hostname $hostname
     * @param Active $active
     * @param ShortCodeMinimumLength $shortCodeMinimumLength
     * @param ShortCodePassKey $shortCodePassKey
     * @param ShortCodeAllowedCharacters $shortCodeAllowedCharacters
     *
     * @return CreateNewSite
     */
    public static function create(SiteId $siteId, Hostname $hostname, Active $active, ShortCodeMinimumLength $shortCodeMinimumLength, ShortCodePassKey $shortCodePassKey, ShortCodeAllowedCharacters $shortCodeAllowedCharacters)
    {
        return new self(
            [
                'siteId' => $siteId->toNative(),
                'hostname' => $hostname->toNative(),
                'active' => $active->toNative(),
                'shortCodeMinimumLength' => $shortCodeMinimumLength->toNative(),
                'shortCodePassKey' => $shortCodePassKey->toNative(),
                'shortCodeAllowedCharacters' => $shortCodeAllowedCharacters->toNative()
            ]
        );
    }

    /**
     * @return SiteId
     */
    public function siteId()
    {
        return SiteId::fromNative($this->payload['siteId']);
    }

    /**
     * @return Hostname
     */
    public function hostname()
    {
        return Hostname::fromNative($this->payload['hostname']);
    }

    /**
     * @return bool
     */
    public function active()
    {
        return $this->payload['active'];
    }

    /**
     * @return int
     */
    public function shortCodeMinimumLength()
    {
        return ShortCodeMinimumLength::fromNative($this->payload['shortCodeMinimumLength']);
    }

    /**
     * @return StringLiteral
     */
    public function shortCodePassKey()
    {
        return ShortCodePassKey::fromNative($this->payload['shortCodePassKey']);
    }

    /**
     * @return StringLiteral
     */
    public function shortCodeAllowedCharacters()
    {
        return ShortCodeAllowedCharacters::fromNative($this->payload['shortCodeAllowedCharacters']);
    }
}
