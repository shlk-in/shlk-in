<?php
namespace ShortLinkApiBundle\Model\Site\Event;

use Assert\Assertion;
use GuyRadford\ValueObject\Hostname;
use GuyRadford\ValueObject\StringLiteral;
use Prooph\EventSourcing\AggregateChanged;
use ShortLinkApiBundle\ValueObject\ShortCodeAllowedCharacters;
use ShortLinkApiBundle\ValueObject\ShortCodeMinimumLength;
use ShortLinkApiBundle\ValueObject\ShortCodePassKey;
use ShortLinkApiBundle\ValueObject\SiteId;

/**
 * Class SiteWasCreated
 *
 * @package ShortLinkApiBundle\Model\Site\Event
 */
final class SiteWasCreated extends AggregateChanged
{
    /**
     * @var SiteId
     */
    private $siteId;

    /**
     * @var Hostname
     */
    private $hostname;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var int
     */
    private $shortCodeMinimumLength;

    /**
     * @var StringLiteral
     */
    private $shortCodePassKey;

    /**
     * @var StringLiteral
     */
    private $shortCodeAllowedCharacters;

    /**
     * @param SiteId $siteId
     * @param Hostname $hostname
     * @param bool $active
     * @param ShortCodeMinimumLength $shortCodeMinimumLength
     * @param ShortCodePassKey $shortCodePassKey
     * @param ShortCodeAllowedCharacters $shortCodeAllowedCharacters
     *
     * @return SiteWasCreated
     */
    public static function withData(SiteId $siteId, Hostname $hostname, $active, ShortCodeMinimumLength $shortCodeMinimumLength, ShortCodePassKey $shortCodePassKey, ShortCodeAllowedCharacters $shortCodeAllowedCharacters)
    {
        //        Assertion::string($hostname);

        $event = self::occur(
            $siteId->toNative(),
            [
                'hostname' => $hostname->toNative(),
                'active' => $active,
                'short_code_minimum_length' => $shortCodeMinimumLength->toNative(),
                'short_code_pass_key' => $shortCodePassKey->toNative(),
                'short_code_allowed_characters' => $shortCodeAllowedCharacters->toNative(),
            ]
        );

        $event->siteId = $siteId;
        $event->hostname = $hostname;
        $event->active = $active;
        $event->shortCodeMinimumLength = $shortCodeMinimumLength;
        $event->shortCodePassKey = $shortCodePassKey;
        $event->shortCodeAllowedCharacters = $shortCodeAllowedCharacters;

        return $event;
    }

    /**
     * @return SiteId
     */
    public function siteId()
    {
        if ($this->siteId === null) {
            $this->siteId = SiteId::fromNative($this->aggregateId());
        }

        return $this->siteId;
    }

    /**
     * @return Hostname
     */
    public function hostname()
    {
        if ($this->hostname === null) {
            $this->hostname = Hostname::fromNative($this->payload['hostname']);
        }

        return $this->hostname;
    }

    /**
     * @return bool
     */
    public function active()
    {
        if ($this->active === null) {
            $this->active = $this->payload['active'];
        }

        return $this->active;
    }

    /**
     * @return int
     */
    public function shortCodeMinimumLength()
    {
        if ($this->shortCodeMinimumLength === null) {
            $this->shortCodeMinimumLength = ShortCodeMinimumLength::fromNative($this->payload['short_code_minimum_length']);
        }

        return $this->shortCodeMinimumLength;
    }

    /**
     * @return StringLiteral
     */
    public function shortCodePassKey()
    {
        if ($this->shortCodePassKey === null) {
            $this->shortCodePassKey = ShortCodePassKey::fromNative($this->payload['short_code_pass_key']);
        }

        return $this->shortCodePassKey;
    }

    /**
     * @return StringLiteral
     */
    public function shortCodeAllowedCharacters()
    {
        if ($this->shortCodeAllowedCharacters === null) {
            $this->shortCodeAllowedCharacters = ShortCodeAllowedCharacters::fromNative($this->payload['short_code_allowed_characters']);
        }

        return $this->shortCodeAllowedCharacters;
    }
}
