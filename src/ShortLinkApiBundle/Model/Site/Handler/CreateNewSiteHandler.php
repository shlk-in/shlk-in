<?php
namespace ShortLinkApiBundle\Model\Site\Handler;

use ShortLinkApiBundle\Model\Site\Command\CreateNewSite;
use ShortLinkApiBundle\Model\Site\Exception\SiteAlreadyExists;
use ShortLinkApiBundle\Model\Site\Site;
use ShortLinkApiBundle\Model\Site\SiteCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CreateNewSiteHandler
 *
 * @package ShortLinkApiBundle\Model\Site\Handler
 */
final class CreateNewSiteHandler
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var SiteCollection
     */
    private $siteCollection;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->siteCollection = $this->container->get('site_collection');
    }

    /**
     * @param CreateNewSite $command
     */
    public function __invoke(CreateNewSite $command)
    {

        //does site already exist?
        $siteFinder = $this->container->get('shlkin.site_projection.site_finder');

        $sites = $siteFinder->findActiveSiteByHostname($command->hostname());
        if (count($sites)) {
            throw SiteAlreadyExists::withHostname($command->hostname());
        }

        $site = Site::createNew(
            $command->siteId(),
            $command->hostname(),
            $command->active(),
            $command->shortCodeMinimumLength(),
            $command->shortCodePassKey(),
            $command->shortCodeAllowedCharacters()
        );

        $this->siteCollection->add($site);
    }
}
