<?php
namespace ShortLinkApiBundle\Model\Site\Exception;

use GuyRadford\ValueObject\Hostname;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class ShortLinkNotFound
 *
 * @package ShortLinkApiBundle\Model\ShortLink
 */
final class SiteAlreadyExists extends HttpException
{
    /**
     * @param Hostname $hostname
     *
     * @return SiteAlreadyExists
     *
     */
    public static function withHostname(Hostname $hostname)
    {
        return new static(400, sprintf('Site with hostname %s already exists.', $hostname->toNative()));
    }
}
