<?php
namespace ShortLinkApiBundle\Model\Site\Exception;

use GuyRadford\ValueObject\Hostname;
use ShortLinkApiBundle\Exception\NotFound;
use ShortLinkApiBundle\ValueObject\SiteId;

/**
 * Class ShortLinkNotFound
 *
 * @package ShortLinkApiBundle\Model\ShortLink
 */
final class SiteNotFound extends NotFound
{
    /**
     * @param SiteId $siteId
     *
     * @return SiteNotFound
     *
     */
    public static function withSiteId(SiteId $siteId)
    {
        return new static(404, sprintf('Site with id %s cannot be found.', $siteId->toNative()));
    }

    /**
     * @param Hostname $hostname
     *
     * @return SiteNotFound
     */
    public static function forHostname(Hostname $hostname)
    {
        return new static(404, sprintf('Site for hostname %s cannot be found.', $hostname->toNative()));
    }
}
