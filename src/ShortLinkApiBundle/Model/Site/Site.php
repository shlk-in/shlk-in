<?php
namespace ShortLinkApiBundle\Model\Site;

use GuyRadford\ValueObject\Hostname;
use Prooph\EventSourcing\AggregateRoot;
use ShortLinkApiBundle\Model\Site\Event\SiteWasCreated;
use ShortLinkApiBundle\ValueObject\ShortCodeAllowedCharacters;
use ShortLinkApiBundle\ValueObject\ShortCodeMinimumLength;
use ShortLinkApiBundle\ValueObject\ShortCodePassKey;
use ShortLinkApiBundle\ValueObject\ShortUrl;
use ShortLinkApiBundle\ValueObject\ShortLinkCode;
use ShortLinkApiBundle\ValueObject\SiteId;

/**
 * Class Site
 *
 * A user manages Todos on his or her TodoList. Each user is identified by her user id, has a name and an email address.
 *
 * @package ShortLinkApiBundle\Model\Site
 */
final class Site extends AggregateRoot
{
    /**
     * @var SiteId
     */
    private $siteId;

    /**
     * @var Hostname
     */
    private $hostname;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var ShortCodeMinimumLength
     */
    private $shortCodeMinimumLength;

    /**
     * @var ShortCodePassKey
     */
    private $shortCodePassKey;

    /**
     * @var ShortCodeAllowedCharacters
     */
    private $shortCodeAllowedCharacters;

    /**
     * @param SiteId $siteId
     * @param Hostname $hostname
     * @param bool $active
     * @param ShortCodeMinimumLength $shortCodeMinimumLength
     * @param ShortCodePassKey $shortCodePassKey
     * @param ShortCodeAllowedCharacters $shortCodeAllowedCharacters
     *
     * @return Site
     */
    public static function createNew(SiteId $siteId, Hostname $hostname, $active, ShortCodeMinimumLength $shortCodeMinimumLength, ShortCodePassKey $shortCodePassKey, ShortCodeAllowedCharacters $shortCodeAllowedCharacters)
    {
        $self = new self();

        $self->recordThat(SiteWasCreated::withData($siteId, $hostname, $active, $shortCodeMinimumLength, $shortCodePassKey, $shortCodeAllowedCharacters));

        return $self;
    }

    /**
     * @return SiteId
     */
    public function SiteId()
    {
        return $this->siteId;
    }

    /**
     * @return Hostname
     */
    public function Hostname()
    {
        return $this->hostname;
    }

    /**
     * @return ShortCodeMinimumLength
     */
    public function ShortCodeMinimumLength()
    {
        return $this->shortCodeMinimumLength;
    }

    /**
     * @return ShortCodePassKey
     */
    public function ShortCodePassKey()
    {
        return $this->shortCodePassKey;
    }

    /**
     * @return ShortCodeAllowedCharacters
     */
    public function ShortCodeAllowedCharacters()
    {
        return $this->shortCodeAllowedCharacters;
    }

    /**
     * get the full short URL.
     *
     * @param ShortLinkCode $shortLinkCode
     *
     * @return string
     */
    public function getShortUrl(ShortLinkCode $shortLinkCode)
    {
        return ShortUrl::fromNative(sprintf('http://%s/%s', $this->Hostname()->toNative(), $shortLinkCode->toNative()));
    }

    /**
     * @return string representation of the unique identifier of the aggregate root
     */
    protected function aggregateId()
    {
        return $this->siteId->toNative();
    }

    /**
     * @param SiteWasCreated $event
     */
    protected function whenSiteWasCreated(SiteWasCreated $event)
    {
        $this->siteId = $event->siteId();
        $this->hostname = $event->hostname();
        $this->active = $event->active();
        $this->shortCodeMinimumLength = $event->shortCodeMinimumLength();
        $this->shortCodePassKey = $event->shortCodePassKey();
        $this->shortCodeAllowedCharacters = $event->shortCodeAllowedCharacters();
    }

    public function toArray()
    {
        return [
            'siteId' => $this->siteId->toNative(),
            'hostname' => $this->hostname->toNative(),
            'active' => $this->active,
            'shortCodeMinimumLength' => $this->shortCodeMinimumLength->toNative(),
            'shortCodePassKey' => $this->shortCodePassKey->toNative(),
            'shortCodeAllowedCharacters' => $this->shortCodeAllowedCharacters->toNative(),
        ];
    }
}
