<?php
namespace ShortLinkApiBundle\Model\Site;

use ShortLinkApiBundle\ValueObject\SiteId;

/**
 * Interface SiteCollection
 *
 * @package ShortLinkApiBundle\Model\Site
 */
interface SiteCollection
{
    /**
     * @param Site $site
     *
     * @return void
     */
    public function add(Site $site);

    /**
     * @param SiteId $siteId
     *
     * @return Site
     */
    public function get(SiteId $siteId);
}
