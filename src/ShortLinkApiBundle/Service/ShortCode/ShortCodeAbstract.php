<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 24/06/2017
 * Time: 14:38
 */
namespace ShortLinkApiBundle\Service\ShortCode;

use Assert\Assertion;

abstract class ShortCodeAbstract
{
    /**
     * @param string $shortCode
     * @param string $allowedCharacters
     */
    protected function validateInputCodeAgainstAllowedCharacters($shortCode, $allowedCharacters)
    {
        for ($i=0; $i<strlen($shortCode); $i++) {
            $letter = $shortCode[$i];
            Assertion::contains($allowedCharacters, $letter);
        }
    }
}
