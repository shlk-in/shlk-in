<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 05/05/2017
 * Time: 21:07
 */
namespace ShortLinkApiBundle\Service\ShortCode;

interface ShortCodeInterface
{
    /**
     * @param int $index
     * @param int $minLength
     * @param string $passKey
     * @param string $allowedCharacters
     *
     * @return string
     */
    public function indexToShortCode($index, $minLength, $passKey, $allowedCharacters);

    /**
     * @param string $shortCode
     * @param int $minLength
     * @param string $passKey
     * @param string $allowedCharacters
     *
     * @return int
     */
    public function shortCodeToIndex($shortCode, $minLength, $passKey, $allowedCharacters);
}
