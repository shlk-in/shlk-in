<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 10/05/2017
 * Time: 21:36
 */
namespace ShortLinkApiBundle\Service\NextInteger;

use Doctrine\DBAL\Schema\Schema;
use GuyRadford\ValueObject\Uuid;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\DBAL\Connection;

class DatabaseNextInteger implements NextIntegerInterface
{
    const TABLE = 'next_integer';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * NextIntegerInterface constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->connection = $container->get('database_connection');
    }

    /**
     * @param Uuid $indexId
     *
     * @return int
     */
    public function getNextInteger(Uuid $indexId)
    {
        $this->connection->beginTransaction();

        $row = $this->connection->fetchAssoc('SELECT * FROM ' . self::TABLE . ' WHERE index_id = ?', array($indexId->toNative()));

        if (false === $row) {
            $this->setInitialInteger($indexId, 1);
            $this->connection->commit();

            return 1;
        }

        $nextInteger = intval($row['last_used_integer']) + 1;

        $this->connection->update(
            self::TABLE,
            [
                'last_used_integer' => $nextInteger
            ],
            [
                'index_id' => $indexId->toNative()
            ]
        );

        $this->connection->commit();

        return $nextInteger;
    }

    /**
     * @param Uuid $indexId
     * @param int $lastUsedInteger
     *
     * @return mixed
     */
    public function setInitialInteger(Uuid $indexId, $lastUsedInteger = 0)
    {
        $this->connection->insert(
            self::TABLE,
            [
                'index_id' => $indexId->toNative(),
                'last_used_integer' => $lastUsedInteger
            ]
        );
    }

    /**
     * Create tables required for projection
     *
     * @param Schema $schema
     */
    public static function createTable(Schema $schema)
    {
        $table = $schema->createTable(self::TABLE);

        $table->addColumn('index_id', 'guid');
        $table->addColumn('last_used_integer', 'integer', array('unsigned' => true));
        $table->setPrimaryKey(['index_id']);
    }

    /**
     * Drop projection table
     *
     * @param Schema $schema
     */
    public static function dropTable(Schema $schema)
    {
        $schema->dropTable(self::TABLE);
    }
}
