<?php

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 10/05/2017
 * Time: 21:30
 */
namespace ShortLinkApiBundle\Service\NextInteger;

use GuyRadford\ValueObject\Uuid;
use Symfony\Component\DependencyInjection\ContainerInterface;

interface NextIntegerInterface
{
    /**
     * NextIntegerInterface constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container);

    /**
     * @param Uuid $indexId
     *
     * @return int
     */
    public function getNextInteger(Uuid $indexId);

    /**
     * @param Uuid $indexId
     * @param int $lastUsedInteger
     *
     * @return mixed
     */
    public function setInitialInteger(Uuid $indexId, $lastUsedInteger = 0);
}
