<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 10/06/2017
 * Time: 16:26
 */
namespace ShortLinkApiBundle\ValueObject;

use GuyRadford\ValueObject\Iso8601DateTime;

class VisitDateTime extends Iso8601DateTime
{
}
