<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 11/06/2017
 * Time: 10:54
 */
namespace ShortLinkApiBundle\ValueObject;

use GuyRadford\ValueObject\IPAddress;

class VisitorsIPAddress extends IPAddress
{
}
