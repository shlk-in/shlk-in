<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 08/04/2017
 * Time: 08:55
 */
namespace ShortLinkApiBundle\ValueObject;

use GuyRadford\ValueObject\Integer;

/**
 * Class SiteId
 *
 * The SiteId identifies a Site being used.
 *
 * @package ShortLinkApiBundle\Model\Site
 */
final class ShortCodeMinimumLength extends Integer
{
}
