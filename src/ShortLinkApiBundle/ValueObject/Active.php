<?php
/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 08/04/2017
 * Time: 08:55
 */
namespace ShortLinkApiBundle\ValueObject;

use GuyRadford\ValueObject\Boolean;

/**
 * Class Active
 *
 * This is a boolean value to note if hte site is active or not.
 *
 * @package ShortLinkApiBundle\ValueObject
 */
final class Active extends Boolean
{
}
