<?php
/*
 * This file is part of the prooph/service-bus.
 * (c) 2014-2015 prooph software GmbH <contact@prooph.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Date: 05/23/15 - 6:22 PM
 */
namespace ShortLinkApiBundle\Lib;

use Prooph\Common\Event\ActionEvent;
use Prooph\Common\Event\ActionEventEmitter;
use Prooph\Common\Event\ActionEventListenerAggregate;
use Prooph\Common\Event\DetachAggregateHandlers;
use Prooph\ServiceBus\MessageBus;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SingleHandlerRouter
 *
 * @package Prooph\ServiceBus\Router
 *
 * @author Alexander Miertsch <kontakt@codeliner.ws>
 */
class CommandRouter implements ActionEventListenerAggregate
{
    use DetachAggregateHandlers;

    protected $container;
    protected $logger;

    /**
     * @var array[messageName => messageHandler]
     */
    protected $messageMap = [];

    /**
     * @var string
     */
    protected $tmpMessageName;

    /**
     * @param null|array[messageName => messageHandler] $commandMap
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->logger = $container->get('logger');
    }

    /**
     * @param ActionEventEmitter $events
     *
     * @return void
     */
    public function attach(ActionEventEmitter $events)
    {
        $this->trackHandler($events->attachListener(MessageBus::EVENT_ROUTE, [$this, 'onRouteMessage']));
    }

    /**
     * @param ActionEvent $actionEvent
     */
    public function onRouteMessage(ActionEvent $actionEvent)
    {
        $messageName = (string)$actionEvent->getParam(MessageBus::EVENT_PARAM_MESSAGE_NAME);

        if (empty($messageName)) {
            return;
        }

        $messageNameParts = explode('\\', $messageName);

        $messageNameParts[3] = 'Handler';
        $messageNameParts[4] .= 'Handler';

        $handler = implode('\\', $messageNameParts);

        if (!class_exists($handler)) {
            $this->container->get('logger')->error(sprintf("Handler for '%s' not found.", $handler));

            return;
        }

        $this->logger->debug("Routing '{$messageName}' > '{$handler}' ");

        $handlerObject = new $handler($this->container);
        $actionEvent->setParam(MessageBus::EVENT_PARAM_MESSAGE_HANDLER, $handlerObject);
    }
}
