<?php
namespace ShlkInTest\ShortLinkApiBundle\Controller;

use GuyRadford\ValueObject\Uuid;
use ShortLinkApiBundle\Service\NextInteger\DatabaseNextInteger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DatabaseNextIntegerTest extends KernelTestCase
{
    /**
     * @var DatabaseNextInteger
     */
    protected $service;

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer();

        $this->service = new DatabaseNextInteger($this->container);
    }

    /**
     * @test
     */
    public function new_call_to_getNextInteger_will_return_1_then_2_3_4()
    {
        $uuid = Uuid::generate();

        $index = $this->service->getNextInteger($uuid);
        $this->assertEquals(1, $index);

        $index = $this->service->getNextInteger($uuid);
        $this->assertEquals(2, $index);

        $index = $this->service->getNextInteger($uuid);
        $this->assertEquals(3, $index);

        $index = $this->service->getNextInteger($uuid);
        $this->assertEquals(4, $index);
    }

    /**
     * @test
     */
    public function set_last_used_value_to_9_and_get_next_integer_of_10_11()
    {
        $uuid = Uuid::generate();

        $this->service->setInitialInteger($uuid, 9);

        $index = $this->service->getNextInteger($uuid);
        $this->assertEquals(10, $index);

        $index = $this->service->getNextInteger($uuid);
        $this->assertEquals(11, $index);
    }
}
