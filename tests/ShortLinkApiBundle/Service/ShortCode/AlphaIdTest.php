<?php
namespace ShlkInTest\ShortLinkApiBundle\Controller;

use PHPUnit\Framework\TestCase;
use ShortLinkApiBundle\Service\ShortCode\AlphaId;

class AlphaIdTest extends TestCase
{
    /**
     * @var AlphaId
     */
    protected $service;

    public function setUp()
    {
        $this->service = new AlphaId();
    }

    /**
     * @test
     */
    public function create_alpha_id()
    {
        foreach (range(1, 100, 5) as $index) {
            $shortCode = $this->service->indexToShortCode($index, 0, '', 'abcdefghij');

            $this->assertInternalType('string', $shortCode);
            $this->assertEquals($index, $this->service->shortCodeToIndex($shortCode, 0, '', 'abcdefghij'));
        }
    }

    /**
     * @test
     */
    public function create_alpha_id_with_padding_4()
    {
        foreach ([1, 2, 10, 50, 100, 999, 12345] as $index) {
            $shortCode = $this->service->indexToShortCode($index, 4, '', 'abcdefghij');
            $this->assertInternalType('string', $shortCode);
            $this->assertGreaterThanOrEqual(4, strlen($shortCode));
            $this->assertEquals($index, $this->service->shortCodeToIndex($shortCode, 4, '', 'abcdefghij'));
        }
    }

    /**
     * @test
     */
    public function create_alpha_id_with_pass_key()
    {

        // some index may match both with and without pass key
        foreach ([1, 2, 10, 50, 100, 12345] as $index) {
            $shortCode = $this->service->indexToShortCode($index, 0, '', 'abcdefghij');
            $shortCodeWithPassKey = $this->service->indexToShortCode($index, 0, 'testing', 'abcdefghij');
            $this->assertNotEquals($shortCode, $shortCodeWithPassKey);
        }
    }
    /**
     * @test
     */
    public function create_alpha_id_with_different_allowed_characters()
    {

        // some index may match both with and without pass key
        foreach ([1, 2, 10, 50, 100, 12345] as $index) {
            $shortCode = $this->service->indexToShortCode($index, 0, '', 'abcdefghij');
            $shortCodeWithPassKey = $this->service->indexToShortCode($index, 0, '', 'klmnopqrst');
            $this->assertNotEquals($shortCode, $shortCodeWithPassKey);
        }
    }

    /**
     * @test
     */
    public function invalid_alphaId_to_index()
    {
        $this->expectExceptionMessage('Value "0123456789abcdef" does not contain "x".');
        $index = $this->service->shortCodeToIndex('12x34', 5, 'test', '0123456789abcdef');
    }
    /**
     * @test
     */
    public function invalid_alphaId_to_index_too_short()
    {
        $this->expectExceptionMessage('Value "abc" is too short, it should have at least 5 characters, but only has 3 characters.');
        $index = $this->service->shortCodeToIndex('abc', 5, 'test', '0123456789abcdef');
    }
}
