<?php
namespace ShlkInTest\ShortLinkApiBundle\Controller\AuthenticationController;

use ShlkInTest\ShortLinkApiBundle\Controller\BaseControllerTestCase;

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 13/06/2017
 * Time: 21:18
 */
class TokenAuthenticationTest extends BaseControllerTestCase
{
    /**
     * @test
     */
    public function get_jwt_token_without_username()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/token-authentication',
            []
        );

        $this->assert401AuthorisationFailed($client->getResponse(), "Can not authenticate with username ''.");
    }

    /**
     * @test
     */
    public function get_jwt_token_with_invalid_username()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/token-authentication',
            [
                'username' => 'invalid',
            ]
        );

        $this->assert401AuthorisationFailed($client->getResponse(), "Can not authenticate with username 'invalid'.");
    }

    /**
     * @test
     */
    public function get_jwt_token_with_valid_username()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/token-authentication',
            [
                'username' => 'testUser',
            ]
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $jsonResponse = json_decode($client->getResponse()->getContent());
        $this->assertObjectHasAttribute('token', $jsonResponse);
    }

    /**
     * @test
     */
    public function test_missing_autho_token()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/site/',
            [
                'hostname' => 'host.net',
                'active' => true,
                'shortCodeMinimumLength' => 5,
                'shortCodePassKey' => 'test_pass',
                'shortCodeAllowedCharacters' => '0123456789abcdef'
            ],
            [],
            []
        );

        $this->assert401AuthorisationFailed($client->getResponse(), 'Authorization header is required.');
    }

    /**
     * @test
     */
    public function test_invalid_auth_token()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/site/',
            [
                'hostname' => 'host.net',
                'active' => true,
                'shortCodeMinimumLength' => 5,
                'shortCodePassKey' => 'test_pass',
                'shortCodeAllowedCharacters' => '0123456789abcdef'
            ],
            [],
            [
                'HTTP_Authorization' => 'Bearer ' . 'invalid'
            ]
        );

        $this->assert401AuthorisationFailed($client->getResponse(), 'Invalid JWT Token');
    }

    /**
     * @test
     */
    public function create_token_and_make_valid_request()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/token-authentication',
            [
                'username' => 'testUser',
            ]
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $jsonResponse = json_decode($client->getResponse()->getContent());
        $this->assertObjectHasAttribute('token', $jsonResponse);

        $hostname = 'token' . (new \DateTime())->format('YmdHis') . '.net';

        $client->request(
            'POST',
            '/api/site/',
            [
                'hostname' => $hostname,
                'active' => true,
                'shortCodeMinimumLength' => 5,
                'shortCodePassKey' => 'test_pass',
                'shortCodeAllowedCharacters' => '0123456789abcdef'
            ],
            [],
            [
                'HTTP_Authorization' => 'Bearer ' . $jsonResponse->token
            ]
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
