<?php
namespace ShlkInTest\ShortLinkApiBundle\Controller\ShortLinkController;

use GuyRadford\ValueObject\Assert\Assertion;
use GuyRadford\ValueObject\Uuid;
use ShlkInTest\ShortLinkApiBundle\Controller\BaseControllerTestCase;

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 13/06/2017
 * Time: 21:18
 */
class ShortLinkVisitedTest extends BaseControllerTestCase
{
    /**
     * @test
     */
    public function createNewShortLink_and_visit_twice()
    {
        $faker = \Faker\Factory::create();

        $hostname = 'visit' . (new \DateTime())->format('YmdHis') . '.net';
        $longUrl = $faker->url;

        $visitorsIPAddress = $faker->ipv4;
        $visitorsUserAgent = $faker->userAgent;

        //create site
        $siteId = $this->createSite('', $hostname);

        $client = static::createClient();

        $client->request(
            'POST',
            '/api/shortLink/',
            [
                'siteId' => $siteId,
                'longUrl' => $longUrl
            ],
            [],
            $this->getHeaders()
        );

        $shortLinkResponse = json_decode($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertTrue($shortLinkResponse->success);
        $this->assertTrue(Assertion::uuid($shortLinkResponse->shortLinkId));

        $client->request(
            'POST',
            '/api/shortLinkVisited/' . $shortLinkResponse->shortLinkId,
            [
                'visitorsIPAddress' => $visitorsIPAddress,
                'visitorsUserAgent' => $visitorsUserAgent
            ],
            [],
            $this->headers
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request(
            'POST',
            '/api/shortLinkVisited/' . $shortLinkResponse->shortLinkId,
            [
                'visitorsIPAddress' => $visitorsIPAddress,
                'visitorsUserAgent' => $visitorsUserAgent

            ],
            [],
            $this->headers
        );

//        $shortLinkVisitedResponse = json_decode($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request(
            'GET',
            '/api/shortLink/' . $shortLinkResponse->shortLinkId,
            [],
            [],
            $this->headers
        );

        $shortLinkReadResponse = json_decode($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertEquals($shortLinkResponse->shortLinkId, $shortLinkReadResponse->shortLink->shortLinkId);
        $this->assertEquals($siteId, $shortLinkReadResponse->shortLink->siteId);
        $this->assertEquals($longUrl, $shortLinkReadResponse->shortLink->longUrl);

        $this->assertEquals("http://$hostname/{$shortLinkReadResponse->shortLink->shortLinkCode}", $shortLinkReadResponse->shortLink->shortUrl);
        $this->assertEquals(5, strlen($shortLinkReadResponse->shortLink->shortLinkCode));

        $this->assertEquals(2, $shortLinkReadResponse->shortLink->visitTotal);

        $this->assertEquals(
            new \DateTime(),
            (new \DateTime())->createFromFormat(\DateTime::ATOM, $shortLinkReadResponse->shortLink->dateTimeOfLastVisitor),
            '', 2
        );

        $this->assertEquals($visitorsIPAddress, $shortLinkReadResponse->shortLink->ipAddressOfLastVisitor);
        $this->assertEquals($visitorsUserAgent, $shortLinkReadResponse->shortLink->userAgentOfLastVisitor);
    }

    /**
     * @test
     */
    public function shortLinkVisited_with_invalid_uuid()
    {
        $faker = \Faker\Factory::create();

        $client = static::createClient();
        $client->request(
            'POST',
            '/api/shortLinkVisited/' . 'invalid_uuid',
            [
                'visitorsIPAddress' => $faker->ipv4,
                'visitorsUserAgent' => $faker->userAgent
            ],
            [],
            $this->headers
        );

        $this->assert400ValidationError($client->getResponse(), 'Value "invalid_uuid" is not a valid UUID.');
    }

    /**
     * @test
     */
    public function shortLinkVisited_with_invalid_uuid_with_missing_uuid()
    {
        $faker = \Faker\Factory::create();

        $invalidShortLinkId = Uuid::generate()->toNative();

        $client = static::createClient();
        $client->request(
            'POST',
            '/api/shortLinkVisited/' . $invalidShortLinkId,
            [
                'visitorsIPAddress' => $faker->ipv4,
                'visitorsUserAgent' => $faker->userAgent
            ],
            [],
            $this->headers
        );
        $this->assert404NotFound($client->getResponse(), "ShortLink with id {$invalidShortLinkId} cannot be found.");
    }

    public function dataProvider_input_validation()
    {
        $shortLinkId = $this->createShortLink();

        $faker = \Faker\Factory::create();

        return [
            [
                $shortLinkId,
                '12.12.12',
                $faker->userAgent,
                'Value "12.12.12" was expected to be a valid IP address.'
            ],
            [
                $shortLinkId,
                $faker->ipv6,
                123,
                'Value "123" expected to be string, type integer given.'
            ],

            [
                $shortLinkId,
                $faker->ipv4,
                123,
                'Value "123" expected to be string, type integer given.'
            ],

        ];
    }

    /**
     * @test
     * @dataProvider dataProvider_input_validation
     *
     * @param $shortLinkId
     * @param $ipAddress
     * @param $userAgent
     * @param $message
     */
    public function input_validation($shortLinkId, $ipAddress, $userAgent, $message)
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/shortLinkVisited/' . $shortLinkId,
            [
                'visitorsIPAddress' => $ipAddress,
                'visitorsUserAgent' => $userAgent
            ],
            [],
            $this->getHeaders()
        );

        $this->assert400ValidationError($client->getResponse(), $message);
    }
}
