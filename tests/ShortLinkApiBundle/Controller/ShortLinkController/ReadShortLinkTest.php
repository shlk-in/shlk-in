<?php
namespace ShlkInTest\ShortLinkApiBundle\Controller\ShortLinkController;

use GuyRadford\ValueObject\Assert\Assertion;
use GuyRadford\ValueObject\Uuid;
use ShlkInTest\ShortLinkApiBundle\Controller\BaseControllerTestCase;

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 13/06/2017
 * Time: 21:18
 */
class ReadShortLinkTest extends BaseControllerTestCase
{
    /**
     * @test
     */
    public function createNewShortLink_and_read_back()
    {
        $faker = \Faker\Factory::create();

        $longUrl = $faker->url;

        //create site
        $hostname = 'readback' . (new \DateTime())->format('YmdHis') . '.net';

        $siteId = $this->createSite('', $hostname);

        $client = static::createClient();
        $client->request(
            'POST',
            '/api/shortLink/',
            [
                'siteId' => $siteId,
                'longUrl' => $longUrl
            ],
            [],
            $this->headers
        );

        $shortLinkResponse = json_decode($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertTrue($shortLinkResponse->success);
        $this->assertTrue(Assertion::uuid($shortLinkResponse->shortLinkId));

        $client->request(
            'GET',
            '/api/shortLink/' . $shortLinkResponse->shortLinkId,
            [],
            [],
            $this->headers
        );

        $shortLinkReadResponse = json_decode($client->getResponse()->getContent());
        $shortLinkReadResponseArray = json_decode($client->getResponse()->getContent(), true);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertEquals($shortLinkResponse->shortLinkId, $shortLinkReadResponse->shortLink->shortLinkId);
        $this->assertEquals($siteId, $shortLinkReadResponse->shortLink->siteId);
        $this->assertEquals($longUrl, $shortLinkReadResponse->shortLink->longUrl);

        $this->assertEquals("http://$hostname/{$shortLinkReadResponse->shortLink->shortLinkCode}", $shortLinkReadResponse->shortLink->shortUrl);
        $this->assertEquals(5, strlen($shortLinkReadResponse->shortLink->shortLinkCode));

        $this->assertEquals(0, $shortLinkReadResponse->shortLink->visitTotal);
        $this->assertNull($shortLinkReadResponse->shortLink->dateTimeOfLastVisitor);

        $this->assertEquals(10, count($shortLinkReadResponseArray['shortLink']));
    }

    /**
     * @test
     */
    public function read_shortlink_with_invalid_shortLinkId()
    {
        $siteId = $this->createSite();

        $client = static::createClient();
        $client->request(
            'GET',
            '/api/shortLink/' . 'invalid_uuid',
            [],
            [],
            $this->headers
        );

        $this->assert400ValidationError($client->getResponse(), 'Value "invalid_uuid" is not a valid UUID.');
    }

    /**
     * @test
     */
    public function read_shortlink_with_missing_uuid()
    {
        $invalidShortLinkId =Uuid::generate()->toNative();

        $client = static::createClient();
        $client->request(
            'GET',
            '/api/shortLink/' . $invalidShortLinkId,
            [],
            [],
            $this->headers
        );
        $this->assert404NotFound($client->getResponse(), "ShortLink with id {$invalidShortLinkId} cannot be found.");
    }
}
