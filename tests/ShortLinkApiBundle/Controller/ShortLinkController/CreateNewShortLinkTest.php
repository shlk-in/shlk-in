<?php
namespace ShlkInTest\ShortLinkApiBundle\Controller\ShortLinkController;

use GuyRadford\ValueObject\Assert\Assertion;
use GuyRadford\ValueObject\Uuid;
use ShlkInTest\ShortLinkApiBundle\Controller\BaseControllerTestCase;

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 13/06/2017
 * Time: 21:18
 */
class CreateNewShortLinkTest extends BaseControllerTestCase
{
    /**
     * @test
     */
    public function createNewShortLink()
    {
        $faker = \Faker\Factory::create();
        $longUrl = $faker->url;

        //create site
        $siteId = $this->createSite();

        $client = static::createClient();
        $client->request(
            'POST',
            '/api/shortLink/',
            [
                'siteId' => $siteId,
                'longUrl' => $longUrl
            ],
            [],
            $this->headers
        );

        $shortLinkResponse = json_decode($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertTrue($shortLinkResponse->success);
        $this->assertTrue(Assertion::uuid($shortLinkResponse->shortLinkId));
    }

    /**
     * @test
     */
    public function createNewShortLinkx2()
    {
        $faker = \Faker\Factory::create();
        $longUrl1 = $faker->url;
        $longUrl2 = $faker->url;

        //create site
        $siteId = $this->createSite();

        $client = static::createClient();
        $client->request(
            'POST',
            '/api/shortLink/',
            [
                'siteId' => $siteId,
                'longUrl' => $longUrl1
            ],
            [],
            $this->headers
        );

        $shortLinkResponse = json_decode($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertTrue($shortLinkResponse->success);
        $this->assertTrue(Assertion::uuid($shortLinkResponse->shortLinkId));

        $client = static::createClient();
        $client->request(
            'POST',
            '/api/shortLink/',
            [
                'siteId' => $siteId,
                'longUrl' => $longUrl2
            ],
            [],
            $this->headers
        );

        $shortLinkResponse = json_decode($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertTrue($shortLinkResponse->success);
        $this->assertTrue(Assertion::uuid($shortLinkResponse->shortLinkId));
    }

    /**
     * @test
     */
    public function createNewShortLink_with_invalid_hostname()
    {
        $siteId = $this->createSite('invalidhost');

        $client = static::createClient();
        $client->request(
            'POST',
            '/api/shortLink/',
            [
                'siteId' => $siteId,
                'longUrl' => 'invalid_log_url'
            ],
            [],
            $this->headers
        );

        $this->assert400ValidationError($client->getResponse(), 'Value "invalid_log_url" was expected to be a valid URL starting with http or https');
    }

    /**
     * @test
     */
    public function createNewShortLink_with_invalid_site_id()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/shortLink/',
            [
                'siteId' => 'invalid_uuid',
                'longUrl' => 'invalid_log_url'
            ],
            [],
            $this->headers
        );

        $this->assert400ValidationError($client->getResponse(), 'Value "invalid_uuid" is not a valid UUID.');
    }

    /**
     * @test
     */
    public function createNewShortLink_with_missing_site_id()
    {
        $invalidSiteId = Uuid::generate()->toNative();

        $client = static::createClient();
        $client->request(
            'POST',
            '/api/shortLink/',
            [
                'siteId' => $invalidSiteId,
                'longUrl' => 'http://validurl.net'
            ],
            [],
            $this->headers
        );

        $this->assert404NotFound($client->getResponse(), "Site with id {$invalidSiteId} cannot be found.");
    }
}
