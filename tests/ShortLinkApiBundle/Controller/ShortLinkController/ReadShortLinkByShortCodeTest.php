<?php
namespace ShlkInTest\ShortLinkApiBundle\Controller\ShortLinkController;

use GuyRadford\ValueObject\Assert\Assertion;
use GuyRadford\ValueObject\Uuid;
use ShlkInTest\ShortLinkApiBundle\Controller\BaseControllerTestCase;

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 13/06/2017
 * Time: 21:18
 */
class ReadShortLinkByShortCodeTest extends BaseControllerTestCase
{
    /**
     * @test
     */
    public function createNewShortLinkAndReadBackByShortCode()
    {
        $faker = \Faker\Factory::create();

        $hostname = 'createhost' . (new \DateTime())->format('YmdHis') . '.net';

        $longUrl = $faker->url;

        //create site
        $siteId = $this->createSite('', $hostname);

        $client = static::createClient();
        $client->request(
            'POST',
            '/api/shortLink/',
            [
                'siteId' => $siteId,
                'longUrl' => $longUrl
            ],
            [],
            $this->headers
        );

        $shortLinkResponse = json_decode($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertTrue($shortLinkResponse->success);
        $this->assertTrue(Assertion::uuid($shortLinkResponse->shortLinkId));

        $client->request(
            'GET',
            '/api/shortLink/' . $shortLinkResponse->shortLinkId,
            [],
            [],
            $this->headers
        );

        $shortLinkReadResponse = json_decode($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertEquals($shortLinkResponse->shortLinkId, $shortLinkReadResponse->shortLink->shortLinkId);
        $this->assertEquals($siteId, $shortLinkReadResponse->shortLink->siteId);
        $this->assertEquals($longUrl, $shortLinkReadResponse->shortLink->longUrl);

        $this->assertEquals("http://$hostname/{$shortLinkReadResponse->shortLink->shortLinkCode}", $shortLinkReadResponse->shortLink->shortUrl);
        $this->assertEquals(5, strlen($shortLinkReadResponse->shortLink->shortLinkCode));

        $client->request(
            'GET',
            '/api/getShortLink/' . $shortLinkReadResponse->shortLink->siteId . '/' . $shortLinkReadResponse->shortLink->shortLinkCode,
            [],
            [],
            $this->headers
        );

        $shortLinkReadResponseFromShortCode = json_decode($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertEquals($shortLinkReadResponse, $shortLinkReadResponseFromShortCode);
    }

    /**
     * @test
     */
    public function read_shortlink_with_invalid_short_code()
    {
        $siteId = $this->createSite('invalid');

        $client = static::createClient();
        $client->request(
            'GET',
            '/api/getShortLink/' . $siteId . '/xyz',
            [],
            [],
            $this->headers
        );

        $this->assert404NotFound($client->getResponse(), 'ShortLink with short code xyz cannot be found.');
    }

    /**
     * @test
     */
    public function read_shortlink_with_missing_short_code()
    {
        $siteId = $this->createSite('missing');

        $invalidShortLinkId = 'abc123';

        $client = static::createClient();
        $client->request(
            'GET',
            '/api/getShortLink/' . $siteId . '/' . $invalidShortLinkId,
            [],
            [],
            $this->headers
        );
        $this->assert404NotFound($client->getResponse(), "ShortLink with short code {$invalidShortLinkId} cannot be found.");
    }
}
