<?php
namespace ShlkInTest\ShortLinkApiBundle\Controller\SiteController;

use ShlkInTest\ShortLinkApiBundle\Controller\BaseControllerTestCase;

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 13/06/2017
 * Time: 21:30
 */
class FindSiteTest extends BaseControllerTestCase
{
    /**
     * @test
     */
    public function siteFind()
    {
        $hostname = 'findhost' . (new \DateTime())->format('YmdHis') . '.net';
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/site/',
            [
//                "siteId" => null,
                'hostname'                   => $hostname,
                'active'                     => true,
                'shortCodeMinimumLength'     => 5,
                'shortCodePassKey'           => 'test_pass',
                'shortCodeAllowedCharacters' => '0123456789abcdef'
            ],
            [],
            $this->headers
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $createResponse = json_decode($client->getResponse()->getContent());

        $client->request(
            'GET',
            '/api/siteFind/',
            [
                'hostname' => $hostname
            ],
            [],
            $this->headers
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $readResponse = json_decode($client->getResponse()->getContent());

        $this->assertTrue($readResponse->success);

        $this->assertEquals($createResponse->siteId, $readResponse->site->siteId);
        $this->assertEquals($hostname, $readResponse->site->hostname);
        $this->assertEquals(true, $readResponse->site->active);
        $this->assertEquals(5, $readResponse->site->shortCodeMinimumLength);
        $this->assertEquals('test_pass', $readResponse->site->shortCodePassKey);
        $this->assertEquals('0123456789abcdef', $readResponse->site->shortCodeAllowedCharacters);
    }

    /**
     * @test
     */
    public function siteFind_invalid_host()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/api/siteFind/',
            [
                'hostname' => 'invalid_host'
            ],
            [],
            $this->headers
        );

        $this->assert400ValidationError($client->getResponse(), 'Value "invalid_host" is not a valid hostname.');
    }

    /**
     * @test
     */
    public function siteFind_with_missing_host()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/api/siteFind/',
            [
                'hostname' => 'host.net'
            ],
            [],
            $this->headers
        );

        $this->assert404NotFound($client->getResponse(), 'Site for hostname host.net cannot be found.');
    }
}
