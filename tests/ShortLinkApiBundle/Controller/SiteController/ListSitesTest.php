<?php
namespace ShlkInTest\ShortLinkApiBundle\Controller\SiteController;

use GuyRadford\ValueObject\Assert\Assertion;
use ShlkInTest\ShortLinkApiBundle\Controller\BaseControllerTestCase;

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 13/06/2017
 * Time: 21:30
 */
class ListSitesTest extends BaseControllerTestCase
{
    /**
     * @test
     */
    public function listAction()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            '/api/site/',
            [],
            [],
            $this->headers
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $response = json_decode($client->getResponse()->getContent());

        $this->assertTrue($response->success);
        $this->assertObjectHasAttribute('sites', $response);
        $this->assertInternalType('array', $response->sites);

        if (count($response->sites)) {
            $this->assertTrue(Assertion::uuid($response->sites[0]->siteId));
            $this->assertTrue(Assertion::isHostname($response->sites[0]->hostname));
            $this->assertTrue(Assertion::boolean($response->sites[0]->active));

            $this->assertObjectHasAttribute('shortCodeMinimumLength', $response->sites[0]);
            $this->assertInternalType('int', $response->sites[0]->shortCodeMinimumLength);

            $this->assertObjectHasAttribute('shortCodePassKey', $response->sites[0]);
            $this->assertInternalType('string', $response->sites[0]->shortCodePassKey);

            $this->assertObjectHasAttribute('shortCodeAllowedCharacters', $response->sites[0]);
            $this->assertInternalType('string', $response->sites[0]->shortCodeAllowedCharacters);
        }
    }
}
