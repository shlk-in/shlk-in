<?php
namespace ShlkInTest\ShortLinkApiBundle\Controller\SiteController;

use GuyRadford\ValueObject\Assert\Assertion;
use GuyRadford\ValueObject\Uuid;
use ShlkInTest\ShortLinkApiBundle\Controller\BaseControllerTestCase;

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 13/06/2017
 * Time: 21:30
 */
class GetSiteDetailsTest extends BaseControllerTestCase
{
    /**
     * @test
     */
    public function getSiteDetails()
    {
        $hostname = 'readhost' . (new \DateTime())->format('YmdHis') . '.net';
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/site/',
            [
//                "siteId" => null,
                'hostname' => $hostname,
                'active' => true,
                'shortCodeMinimumLength' => 5,
                'shortCodePassKey' => 'test_pass',
                'shortCodeAllowedCharacters' => '0123456789abcdef'
            ],
            [],
            $this->headers
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $createResponse = json_decode($client->getResponse()->getContent());

        $client->request(
            'GET',
            '/api/site/' . $createResponse->siteId,
            [],
            [],
            $this->headers
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $readResponse = json_decode($client->getResponse()->getContent());

        $this->assertTrue($readResponse->success);

        $this->assertEquals($createResponse->siteId, $readResponse->site->siteId);
        $this->assertEquals($hostname, $readResponse->site->hostname);
        $this->assertTrue(Assertion::boolean($readResponse->site->active));
        $this->assertEquals(true, $readResponse->site->active);
        $this->assertEquals(5, $readResponse->site->shortCodeMinimumLength);
        $this->assertEquals('test_pass', $readResponse->site->shortCodePassKey);
        $this->assertEquals('0123456789abcdef', $readResponse->site->shortCodeAllowedCharacters);
    }

    /**
     * @test
     */
    public function getSiteDetails_for_missing_site_id()
    {
        $missingSiteId = Uuid::generate();

        $client = static::createClient();

        $client->request(
            'GET',
            '/api/site/' . $missingSiteId->toNative(),
            [],
            [],
            $this->headers
        );

        $this->assert404NotFound($client->getResponse(), "Site with id {$missingSiteId->toNative()} cannot be found.");
    }

    /**
     * @test
     */
    public function getSiteDetails_for_invalid_site_id()
    {
        $client = static::createClient();

        $client->request(
            'GET',
            '/api/site/' . 'invalid-uuid',
            [],
            [],
            $this->headers
        );

        $this->assert400ValidationError($client->getResponse(), 'Value "invalid-uuid" is not a valid UUID.');
    }
}
