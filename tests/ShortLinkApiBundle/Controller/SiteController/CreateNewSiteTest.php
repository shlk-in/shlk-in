<?php
namespace ShlkInTest\ShortLinkApiBundle\Controller\SiteController;

use GuyRadford\ValueObject\Assert\Assertion;
use ShlkInTest\ShortLinkApiBundle\Controller\BaseControllerTestCase;

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 13/06/2017
 * Time: 21:30
 */
class CreateNewSiteTest extends BaseControllerTestCase
{
    /**
     * @test
     */
    public function createNewSite()
    {
        $hostname = 'createhost' . (new \DateTime())->format('YmdHis') . '.net';
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/site/',
            [
                'hostname' => $hostname,
                'active' => true,
                'shortCodeMinimumLength' => 5,
                'shortCodePassKey' => 'test_pass',
                'shortCodeAllowedCharacters' => '0123456789abcdef'
            ],
            [],
            $this->headers
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $createResponse = json_decode($client->getResponse()->getContent());

        $this->assertTrue($createResponse->success);
        $this->assertTrue(Assertion::uuid($createResponse->siteId));
    }

    public function dataProvider_createNewSite()
    {
        $hostname = 'valid' . (new \DateTime())->format('YmdHis') . '.net';

        return [
            [
                [
                    'hostname' => 'invalid_data',
                    'active' => true,
                    'shortCodeMinimumLength' => 5,
                    'shortCodePassKey' => 'test_pass',
                    'shortCodeAllowedCharacters' => '0123456789abcdef'
                ],
                'Value "invalid_data" is not a valid hostname.'
            ],
            [
                [
                    'hostname' => $hostname,
                    'active' => 'active',
                    'shortCodeMinimumLength' => 5,
                    'shortCodePassKey' => 'test_pass',
                    'shortCodeAllowedCharacters' => '0123456789abcdef'
                ],
                'Value "active" is not a boolean.'
            ],
            [
                [
                    'hostname' => $hostname,
                    'active' => true,
                    'shortCodeMinimumLength' => 'invalid',
                    'shortCodePassKey' => 'test_pass',
                    'shortCodeAllowedCharacters' => '0123456789abcdef'
                ],
                'Value "invalid" is not an integer or a number castable to integer.'
            ],
            [
                [
                    'hostname' => $hostname,
                    'active' => 'true',
                    'shortCodeMinimumLength' => 5,
                    'shortCodePassKey' => 'test_pass',
                    'shortCodeAllowedCharacters' => '0123456789abcdef'
                ],
                'Value "true" is not a boolean.'
            ],

        ];
    }

    /**
     * @test
     * @dataProvider dataProvider_createNewSite
     *
     * @param $formParams
     * @param $errorMessage
     */
    public function invalid_createNewSite($formParams, $errorMessage)
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/site/',
            $formParams,
            [],
            $this->headers
        );

        $this->assert400ValidationError($client->getResponse(), $errorMessage);
    }

    public function duplicate_hostname()
    {
        $hostname = 'duplicate_hostname' . (new \DateTime())->format('YmdHis') . '.net';
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/site/',
            [
                'hostname' => $hostname,
                'active' => true,
                'shortCodeMinimumLength' => 5,
                'shortCodePassKey' => 'test_pass',
                'shortCodeAllowedCharacters' => '0123456789abcdef'
            ],
            [],
            $this->headers
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request(
            'POST',
            '/api/site/',
            [
                'hostname' => $hostname,
                'active' => true,
                'shortCodeMinimumLength' => 5,
                'shortCodePassKey' => 'test_pass',
                'shortCodeAllowedCharacters' => '0123456789abcdef'
            ],
            [],
            $this->headers
        );

        $this->assert400ValidationError($client->getResponse(), "Site with hostname $hostname already exists.");
    }
}
