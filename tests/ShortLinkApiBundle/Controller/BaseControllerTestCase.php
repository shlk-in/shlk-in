<?php
namespace ShlkInTest\ShortLinkApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Created by PhpStorm.
 * User: GuyRadford
 * Date: 13/06/2017
 * Time: 21:30
 */
class BaseControllerTestCase extends WebTestCase
{
    protected $headers;

    public function setUp()
    {
        //        $this->headers['HTTP_Authorization'] = 'Bearer ' .$this->getToken();
        $this->getHeaders();
    }

    protected function getHeaders()
    {
        if (empty($this->headers)) {
            $this->headers['HTTP_Authorization'] = 'Bearer ' . $this->getToken();
        }

        return $this->headers;
    }

    /**
     * @return string
     */
    protected function getToken()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/token-authentication',
            [
                'username' => 'testUser',
            ]
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $jsonResponse = json_decode($client->getResponse()->getContent());
        $this->assertObjectHasAttribute('token', $jsonResponse);

        return $jsonResponse->token;
    }

    /**
     * @param Response $response
     * @param $message
     */
    protected function assert404NotFound(Response $response, $message)
    {
        $this->assertResponseError($response, 404, $message);
    }

    /**
     * @param Response $response
     * @param $message
     */
    protected function assert400ValidationError(Response $response, $message)
    {
        $this->assertResponseError($response, 400, $message);
    }

    /**
     * @param Response $response
     * @param $message
     */
    protected function assert401AuthorisationFailed(Response $response, $message)
    {
        $this->assertResponseError($response, 401, $message);
    }

    /**
     * @param Response $response
     * @param int $code
     * @param string $message
     */
    protected function assertResponseError(Response $response, $code, $message)
    {
        if (500 == $response->getStatusCode()) {
            print_r($response->getContent());
        }

        $this->assertEquals($code, $response->getStatusCode());

        $jsonResponse = json_decode($response->getContent());

        $this->assertFalse($jsonResponse->success);

        $this->assertObjectHasAttribute('error', $jsonResponse);
        $this->assertEquals($message, $jsonResponse->error->message);
    }

    public function createSite($hostnamePrefix = 'createSite', $hostname = null)
    {
        $faker = \Faker\Factory::create();

        if (empty($hostname)) {
            $hostname = $hostnamePrefix . (new \DateTime())->format('YmdHis') . '.net';
        }
        $longUrl = $faker->url;

        //create site

        $client = static::createClient();
        $client->request(
            'POST',
            '/api/site/',
            [
                'hostname' => $hostname,
                'active' => true,
                'shortCodeMinimumLength' => 5,
                'shortCodePassKey' => 'test_pass',
                'shortCodeAllowedCharacters' => '0123456789abcdef'
            ],
            [],
            $this->getHeaders()
        );

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $siteResponse = json_decode($client->getResponse()->getContent());

        return $siteResponse->siteId;
    }

    public function createShortLink()
    {
        $faker = \Faker\Factory::create();

        $longUrl = $faker->url;

        //create site
        $siteId = $this->createSite('shortLink');

        $client = static::createClient();

        $client->request(
            'POST',
            '/api/shortLink/',
            [
                'siteId' => $siteId,
                'longUrl' => $longUrl
            ],
            [],
            $this->getHeaders()
        );

        $shortLinkResponse = json_decode($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertTrue($shortLinkResponse->success);

        return $shortLinkResponse->shortLinkId;
    }
}
